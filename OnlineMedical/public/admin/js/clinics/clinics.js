$(function() {

    $('#create_clinic_form').validate({
        rules : {
            name : {
                required  :  true
            },

            telephone_number : {
                required  :  true,
                // number  :  true
            },

            description   : {
                required  : true,
                maxlength : 255,
            },

            location : {
                required  :  true
            },

            c_category_id : {
                required  : true
            },

            // image : {
            //    required   : true,
            // },
        },

        messages: {
            name : {
               required      : 'You must provide a name for the clinic !',
            },

            telephone_number : {
                required     : 'You must provide telephone number for the clinic !',
                // number      : 'The telephone number must be of the INTEGER type !'
            },

            description     : {
               required     : 'You must provide a description for the clinic !',
            },

            location : {
                required     : 'You must provide a location for the clinic !',
            },

            c_category_id : {
               required     : 'You must choose a category for the blog !'
            }

            // image : {
            //    required     : 'You must provide an image for the blog',
            // },
        },

        submitHandler: function(form) {
            form.submit();
        }
    })


    // CKEDITOR.replace('description');
    // $('#clinic_description').summernote();
})