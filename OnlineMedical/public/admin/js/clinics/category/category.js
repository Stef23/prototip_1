$(function() {

    $('#create_clinic_category_form').validate({
        rules : {
            category : {
                required  :  true
            },
        },

        messages: {
            category : {
               required      : 'You must provide a name for the category !',
            },
        },

        submitHandler: function(form) {
            form.submit();
        }
    })


    // CKEDITOR.replace('description');
    // $('#clinic_description').summernote();
})