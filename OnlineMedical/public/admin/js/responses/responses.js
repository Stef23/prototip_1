$(function() {

    $('#create_response_form').validate({
        rules: {
            'questionnaire_contents[question_response]' : {
                    required : true
                },
            },
        messages: {
            'questionnaire_contents[question_response]' : {
                required : 'You must provide a question',
            },
        },
        submitHandler: function(form) {
            
            form.submit();
            
        }
    });
    
    $('#questionnaire_contents').summernote({
        height: 600,
        dialogsInBody: true,
        callbacks:{
            onInit:function(){
            $('body > .note-popover').hide();
            }
         },
     
    });

})