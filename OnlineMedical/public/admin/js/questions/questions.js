$(function() {

    let answer_counter = 2;

    $(document).on('click', '#add_answer', function(e) {
    
        $('#answers_container').append(`
            <div class="form-group">
                <label class="delete-answer" for="answer_` + answer_counter + `">
                    <i class="fa fa-times" aria-hidden="true"></i>
                </label>

                <input 
                    type="text" 
                    name="choices[` + answer_counter + `][choice]" 
                    class="form-control answer"
                    placeholder="Choice" 
                    id="answer_ ` + answer_counter + `">
            </div>
    
        `);
        answer_counter++;

      });


      $(document).on('click', '.delete-answer', function (e) {

        if($(e.target).hasClass('fa-times')) {
           $(e.target).parent().parent().remove(); 
        } else {
            $(e.target).parent().remove();
        }

    });

    $.validator.addMethod("cRequired", $.validator.methods.required,
    "A choice must be provided");
    $.validator.addClassRules("answer", {cRequired : true});

    $('#create_qusetion_form').validate({
        rules: {
            'questionnaire_contents[question_response]' : 'required',
        },
        messages: {
            'questionnaire_contents[question_response]' : 'You must provide a question',
        },
        submitHandler: function(form) {
            
            // if($('#answers_container').children().length > 0) {    
                form.submit();
            // }
            
        }
    });

})