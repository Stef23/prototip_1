$(function () {

    window.sessionStorage.removeItem('questionnaire_pattern');

    $('#questionnaire_content').html('');
    
    let data_to_send = {};
    
    let questionnaire_pattern = {
        'current_question_response_id' : '',
        'prev_question' : [],
        'prev_question_choice' : []
    } 

    let questionnaire_id = $('#questionnaire_content').attr('data');

    let route = '/api/questionnaires/preview/'+ questionnaire_id;


    $.get(route, data_to_send).then(function(data) {
        let html = create_content(data);

        $('#questionnaire_content').html(html);
    });


    $(document).on('click', '#next', function (e) {
        let route = '/api/questionnaires/preview/' + questionnaire_id + '/jump-to';
        
        let inputs = $('#questionnaire_content').find('input');
    
        if($(inputs).is(':checked')) {
            
            let question_id = $(inputs).filter(':checked').attr('data');
            let choice_id = $(inputs).filter(':checked').attr('id');

            questionnaire_pattern.prev_question.push(question_id);
            questionnaire_pattern.prev_question_choice.push(choice_id);
            
            data_to_send.next_question_response_id = $(inputs).filter(':checked').val();

            $.get(route, data_to_send).then(function(data) {
                let html = create_content(data);
                questionnaire_pattern.current_question_response_id = data[0].id;

                window.sessionStorage.setItem('questionnaire_pattern', JSON.stringify(questionnaire_pattern));

                $('#questionnaire_content').html(html);
            });
            
        } else {
            alert('Must choose an answer')
        }
                
    });

    $(document).on('click', '#back', function (e) {
        let prev_question = questionnaire_pattern.prev_question.pop();
        let prev_question_choice = questionnaire_pattern.prev_question_choice.pop();


        let route = '/api/questionnaires/preview/' + questionnaire_id + '/jump-back';
    
        data_to_send.prev_question_response_id = prev_question;

        $.get(route, data_to_send).then(function(data) {
            let html = create_content(data, prev_question_choice);

            questionnaire_pattern.current_question_response_id = data[0].id;

            window.sessionStorage.setItem('questionnaire_pattern', JSON.stringify(questionnaire_pattern));

            $('#questionnaire_content').html(html);
        });
            
                
    });

    $(document).on('change', '#choices-container input', function(e) {
        if($(e.target).is(':checked')){
            $('.choice').find(`i`).removeClass('fa-check-circle');
            $('.choice').find(`i`).addClass('fa-circle');
            $('.choice').removeClass('selected');
            
            $(e.target).prev().find('i').removeClass(`fa-circle`);
            $(e.target).prev().find('i').addClass(`fa-check-circle`);
            
            $(e.target).prev().addClass(`selected`);
        }
    });



    function create_content(data, prev_question_choice = 0) {
        let choices = '';

        data[0].choices.forEach(choice => {
            choices += `
                <label class="choice ${prev_question_choice == choice.id ? 'selected' : ''}" for="${choice.id}">    
                <i class="${prev_question_choice == choice.id ? 'far fa-check-circle' : 'far fa-circle'}"></i>
                        ${choice.choice}                      
                </label>
                <input ${prev_question_choice == choice.id ? 'checked' : ''} class="d-none" data="${data[0].id}" type="radio" name="choice" id="${choice.id}" value="${choice.jump_to}">
                `
        });
        
        let buttons = `
            <button ${data[0].starter_question === 1 ? 'disabled' : ''} id="back" class="btn btn-outline-success m-2 btn-round">Back</button>
            <button id="next" class="btn btn-outline-success m-2 btn-round">Next</button>
            `

        if(data[0].type == 'response') {
            return html = `            
            <div class="col-md-10 p-4 offset-md-1 d-flex flex-column justify-content-between shadow p-3">
                <div class="row ">
                    
                    <div id="response-container" class="col-md-12 h-100 d-flex flex-column justify-content-center">
                        <p class="response">${data[0].question_response}</p>                    
                    </div>
                    
                </div> 
            </div>
            `;
        } else if(data[0].type == 'question') {
            return html = `
            <div class="col-md-10 offset-md-1 text-center d-flex flex-column justify-content-between shadow p-3">
                <div class="row min-vh-50">
                    <div class="col-md-12">
                        <p class="text-center">Welcome, let's see if we can help you with your ${data[0].questionnaire.name} problem.</p>
                        <small>Please carefully consider the question and choose one of the suggested choices</small>
                    </div>
                    <div class="col-md-6">

                        <div id="question-container" class="h-100 d-flex flex-column justify-content-center align-items-center">
                            <p class="question">${data[0].question_response}</p>                    
                        </div>
                    </div>
                    <div class="col-md-6">

                        <div id="choices-container" class="form-group h-100 d-flex flex-column justify-content-center">
                            ${choices}
                        </div>                    
                    </div>
                </div> 
                
                <div class="row justify-content-center">
                    ${buttons}
                </div>

            </div>
        `            
        }

    }
});