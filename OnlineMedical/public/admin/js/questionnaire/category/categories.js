$(function() {
    // the for-loop is for the edit blade, makrs the previously selected icon
    for (let i = 0; i < $('input[name=icon_id]').length; i++) {
        const element = $('input[name=icon_id]')[i];

        if($(element).is(':checked')) {
            $(element).next().find('.card').addClass('choosen-icon');
            $(element).next().find('h3').removeClass('text-info');            
        }
        
    }


    $('#create_category_form').on('submit', function(e) {
        let category = $('#create_category_form').find('input[name=category]').val();
        
        let icons = $('#create_category_form').find('input[name=icon_id]');
        let icon_id = false;
        for(let i = 0; i < icons.length; i++) {
            if($(icons[i]).is(':checked')) {
                icon_id = $(icons[i]).val();
            }
        }

        if(icon_id == false) {
            $('#icon_container').removeClass('border border-info');
            $('#icon_container').addClass('border border-danger');
            $('#icon_container').next().html('<small class="text-danger">You must choose an icon for your category</small>');
        } else {
            $('#icon_container').addClass('border border-info');
            $('#icon_container').removeClass('border border-danger');
            $('#icon_container').next().html('');
        }

        if(category == "") {
            $('input[name=category]').next().html('<small class="text-danger">You must enter a category</small>')
            $('input[name=category]').addClass('border border-danger');
        } else {
            $('input[name=category]').next().html('');
            $('input[name=category]').removeClass('border border-danger');

        }

        if(icon_id == false || category == "") {
            e.preventDefault();
        }         
        

    });

    $(document).on('click', 'input[name=icon_id]', function(e) {
        $('input[name=icon_id]').next().find('.card').removeClass('choosen-icon');
        $('input[name=icon_id]').next().find('h3').addClass('text-info');
        
        if($(e.target).is(':checked')) {
            $(e.target).next().find('.card').addClass('choosen-icon');
            $(e.target).next().find('h3').removeClass('text-info');            
        }

    })

})