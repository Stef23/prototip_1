$(function() {

    $('#create_questionnaire_form').validate({
        
        rules : {
            name : {
                required  : true
            },

            description   : {
                required  : true,
            },

            q_category_id : {
                required  : true
            },
        },

        messages: {
            name : {
               required     : 'You must provide a name for the questionnaire !',
            },

            description     : {
               required     : 'You must provide a description for the questionnaire !',
            },

            q_category_id : {
               required     : 'You must choose a category for the questionnaire !'
            }
        },

        submitHandler: function(form) {
            form.submit();
        }
    })

})