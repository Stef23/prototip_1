$(function() {

    $('#create_product_form').validate({
        rules : {
            name : {
                required  : true
            },

            short_description : {
                required  : true,
            },

            description   : {
                required  : true,
            },

            p_sub_category_id : {
                required  : true
            },

            // image : {
            //    required   : true,
            // },
        },

        messages: {
            name : {
               required       : 'You must provide a name for the product !',
            },

            short_description : {
                required      : 'You must provide short description for the product !',
            },

            description       : {
               required       : 'You must provide a description for the product !',
            },

            p_sub_category_id : {
               required       : 'You must choose a category for the product !'
            }

            // image : {
            //    required     : 'You must provide an image for the product',
            // },
        },

        submitHandler: function(form) {
            form.submit();
        }
    })


    // CKEDITOR.replace('description');
    $('#product_description').summernote();
})