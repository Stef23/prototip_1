$(function() {
    $('#create_product_sub_category_form').validate({
        rules : {
            sub_category : {
                required : true,
            } 
        },

        messages: {
            sub_category : {
               required : 'You must provide a sub category name',
            } 
        },
        
        submitHandler: function(form) {
            form.submit();
        }
    })
})