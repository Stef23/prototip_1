$(function() {
    $('#create_product_category_form').validate({
        rules : {
            category : {
                required : true,
            } 
        },

        messages: {
            category : {
               required : 'You must provide a name for the product category',
            } 
        },
        
        submitHandler: function(form) {
            form.submit();
        }
    })
})