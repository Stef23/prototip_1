$(function() {
    $('#create_blog_form').validate({
        rules : {
            // cover_image : {
            //    required   : true,
            // },

            title : {
                required  : true
            },

            body : {
                required  : true
            },

            blog_tags : {
                required  : true,
                maxlength : 255,
            },

            b_category_id : {
                required  : true
            },
        },

        messages: {
            // cover_image : {
            //    required     : 'You must provide an image for the blog',
            // },

            title : {
               required     : 'You must provide a title for the blog !',
            },

            body : {
                required    : 'You must provide a body for the blog !',
            },

            blog_tags : {
               required     : 'You must provide tags for the blog !',
               maxlength    : 'The tags may not be greater than 255 characters !',
            },

            b_category_id : {
               required     : 'You must choose a category for the blog !'
            }
        },

        submitHandler: function(form) {
            form.submit();
        }
    })


    // CKEDITOR.replace('description');
    $('#blog_body').summernote({
        height: 600,
        dialogsInBody: true,
        callbacks:{
            onInit:function(){
            $('body > .note-popover').hide();
            }
         },
    });
})