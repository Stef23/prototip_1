$(function() {
    $('#create_blog_category_form').validate({
        rules : {
            category : {
                required : true,
            } 
        },
        
        messages: {
            category : {
               required : 'You must provide a name for the blog category',
            }
        },
        submitHandler: function(form) {
            form.submit();
        }
    })
})