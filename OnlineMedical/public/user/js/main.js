$(function() {
    
    $(document).on('scroll',function () {
        var nav = $(".fixed-top");
        nav.toggleClass('scrolled', $(this).scrollTop() > nav.height());
      });

      $(document).on('click', '.category-btn', function() {
        const category_nav = $('#product_category_nav');
        category_nav.toggleClass('open');
      });
    
      $('.dropdown').hover(function() {
        
          $(this).find('.dropdown-menu').stop(true, true).delay(50).fadeIn(300);
        
        }, function() {
          
          $(this).find('.dropdown-menu').stop(true, true).delay(50).fadeOut(300);
        
        });
    
});