$(function () {
    window.sessionStorage.removeItem('questionnaire_pattern');

    $('#questionnaire_content').html('');
    
    let data_to_send = {};
    
    let questionnaire_pattern = {
        'current_question_response_id' : '',
        'prev_question' : [],
        'prev_question_choice' : []
    } 

    let questionnaire_id = $('#questionnaire_content').attr('data');

    let route = '/api/questionnaires/'+ questionnaire_id;


    $.get(route, data_to_send).then(function(data) {
        let html = create_content(data);

        $('#questionnaire_content').html(html);
    });

    $(document).on('click', '#continue_to_questionnare', function(e) {
        if($('#agree_with_terms').is(':checked')) {
            window.sessionStorage.setItem('agreed_with_terms', 'yes');
            
            let route = '/api/questionnaires/'+ questionnaire_id;
            
            $.get(route, data_to_send).then(function(data) {
                let html = create_content(data);
                $('#questionnaire_content').html(html);
            });            

        } else {
            alert('Please agree with the terms of use if you would like to continue');
        }
    })

    $(document).on('click', '#next', function (e) {
        let route = '/api/questionnaires/' + questionnaire_id + '/jump-to';
        
        let inputs = $('#questionnaire_content').find('input');
    
        if($(inputs).is(':checked')) {
            
            let question_id = $(inputs).filter(':checked').attr('data');
            let choice_id = $(inputs).filter(':checked').attr('id');

            questionnaire_pattern.prev_question.push(question_id);
            questionnaire_pattern.prev_question_choice.push(choice_id);
            
            data_to_send.next_question_response_id = $(inputs).filter(':checked').val();
            
            $('#questionnaire_content').hide();
            $.get(route, data_to_send).then(function(data) {
                let html = create_content(data);
                questionnaire_pattern.current_question_response_id = data[0].id;

                window.sessionStorage.setItem('questionnaire_pattern', JSON.stringify(questionnaire_pattern));
                $('#questionnaire_content').html(html);
                $('#questionnaire_content').show();

            });
            
        } else {
            alert('Must choose an answer')
        }
                
    });

    $(document).on('click', '#back', function (e) {
        let prev_question = questionnaire_pattern.prev_question.pop();
        let prev_question_choice = questionnaire_pattern.prev_question_choice.pop();


        let route = '/api/questionnaires/' + questionnaire_id + '/jump-back';
    
        data_to_send.prev_question_response_id = prev_question;
        
        $('#questionnaire_content').hide();
        
        $.get(route, data_to_send).then(function(data) {
            let html = create_content(data, prev_question_choice);

            questionnaire_pattern.current_question_response_id = data[0].id;

            window.sessionStorage.setItem('questionnaire_pattern', JSON.stringify(questionnaire_pattern));
            $('#questionnaire_content').show();
            $('#questionnaire_content').html(html);
        });
            
                
    });

    $(document).on('change', '#choices-container input', function(e) {
        if($(e.target).is(':checked')){
            $('.choice').find(`i`).removeClass('fa-check-circle');
            $('.choice').find(`i`).addClass('fa-circle');
            $('.choice').removeClass('selected');
            
            $(e.target).prev().find('i').removeClass(`fa-circle`);
            $(e.target).prev().find('i').addClass(`fa-check-circle`);
            
            $(e.target).prev().addClass(`selected`);
        }
    });

    $(document).on('change', '#agree_with_terms', function(e) {
        if($(e.target).is(':checked')) {
            $(e.target).siblings().find('i').removeClass('fa-circle');
            $(e.target).siblings().find('i').addClass('fa-check-circle');
        } else {
            $(e.target).siblings().find('i').addClass('fa-circle');
            $(e.target).siblings().find('i').removeClass('fa-check-circle');
        }
    })

    function create_content(data, prev_question_choice = 0) {
        let choices = '';

        data[0].choices.forEach(choice => {
            choices += `
                <label class="choice ${prev_question_choice == choice.id ? 'selected' : ''}" for="${choice.id}">    
                <i class="${prev_question_choice == choice.id ? 'far fa-check-circle' : 'far fa-circle'}"></i>
                        ${choice.choice}                      
                </label>
                <input ${prev_question_choice == choice.id ? 'checked' : ''} class="d-none" data="${data[0].id}" type="radio" name="choice" id="${choice.id}" value="${choice.jump_to}">
                `
        });
        
        let buttons = `
            <button ${data[0].starter_question === 1 ? 'disabled' : ''} id="back" class="btn btn-outline-success m-2 btn-round">Назад</button>
            <button id="next" class="btn btn-outline-success m-2 btn-round">Следно</button>
            `;
        
        if(window.sessionStorage.getItem('agreed_with_terms') === null) {
            return html = `            
            <div class="col-md-10 offset-md-1 text-center d-flex flex-column justify-content-between shadow p-3 bg-white">
                <div class="row">
                    
                    <div id="terms_of_use-container" class="col-md-6 min-vh-50 d-flex flex-column justify-content-center align-items-center">
                        
                        <p class=" h3 terms_of_use">
                            Ве молиме прочитајте ги внимателно 
                            
                                <a href="#" data-toggle="modal" data-target="#termsOfUseModal">условите на користење</a>
                            
                            пред да продолжите кон прашалникот.
                        </p>                    
                    
                    </div>
                    
                    <div class="col-md-6 min-vh-50 d-flex flex-column justify-content-center align-items-center">
                        
                        <input class="d-none" type="checkbox" id="agree_with_terms">  
                        
                        <label for="agree_with_terms">
                        
                            <i class="far fa-circle"></i> 
                            
                            Се согласувам со условите на користење 
                        
                        </label>

                        <button id="continue_to_questionnare" class="btn btn-success">Продолжи</button>                    
                    </div>
                    
                </div> 
            </div>
            `;
        }
        if(data[0].type == 'response') {
            return html = `

            <div class="col-md-10 my-4 offset-md-1 shadow p-3 bg-white">
                <div class="row">
                    
                    <div id="response-container" class="col-md-12 p-4 d-flex flex-column justify-content-center">
                        <div class="response">${data[0].question_response}</div>                   
                    </div>
                    
                </div> 
            </div>

            `;
        } else if(data[0].type == 'question') {
            return html = `
            <div class="col-md-10 offset-md-1 text-center d-flex flex-column justify-content-between shadow p-3 bg-white">
                <div class="row min-vh-50">

                    <div class="col-md-6">
                        <div id="question-container" class="h-100 d-flex flex-column justify-content-center align-items-center">
                            <p class="question">${data[0].question_response}</p>                    
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div id="choices-container" class="form-group h-100 d-flex flex-column align-items-center justify-content-center">
                            ${choices}
                        </div>                    
                    </div>
                </div> 
                
                <div class="row justify-content-center">
                    ${buttons}
                </div>

            </div>
        `            
        }

    }
});