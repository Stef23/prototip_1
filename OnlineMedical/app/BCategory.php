<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BCategory extends Model
{
    protected $guarded = [];

    public function section() {
        return $this->belongsTo(Section::class, 'section_id');
    }

    public function sub_categories() {
        return $this->hasMany(BSubCategory::class, 'b_category_id');
    }
}
