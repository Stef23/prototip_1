<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BSubCategory extends Model
{
    protected $guarded = [];
    
    public function category() {
        return $this->belongsTo(BCategory::class, 'b_category_id');
    }

    public function blogs() {
        return $this->hasMany(Blog::class);
    }
}
