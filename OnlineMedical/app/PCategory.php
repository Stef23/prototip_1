<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PCategory extends Model
{
    protected $guarded = [];

    public function sub_categories()
    {
        return $this->hasMany(PSubCategory::class);
    }

}
