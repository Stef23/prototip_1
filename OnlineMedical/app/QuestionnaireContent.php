<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuestionnaireContent extends Model
{
    protected $guarded = [];


    public function questionnaire()
    {
        return $this->belongsTo(Questionnaire::class);
    }

    public function choices() 
    {
        return $this->hasMany(QuestionChoice::class, 'question_id');
    }

}
