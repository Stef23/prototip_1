<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Section extends Model
{
    protected $with = ['categories.sub_categories.blogs'];
    
    public function categories() {
        return $this->hasMany(BCategory::class, 'section_id');
    }
}
