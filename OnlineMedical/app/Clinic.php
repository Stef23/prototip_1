<?php

namespace App;

use App\Http\Controllers\Admin\ClinicCategoryController;
use Illuminate\Database\Eloquent\Model;
use App\Utilities\ClinicFilter;

class Clinic extends Model
{
    protected $guarded = [];

    public function c_categories()
    {
        return $this->belongsTo(CCategory::class, 'c_category_id');
    }

    public function scopeFilterBy($query, $filters) {
        $result = new ClinicFilter($query, $filters);
        return $result->applyFilters();
        
    }
}
