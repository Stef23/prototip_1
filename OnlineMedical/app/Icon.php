<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Icon extends Model
{
    public function categories() {
        return $this->hasMany(QCategory::class);
    }
}
