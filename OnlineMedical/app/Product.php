<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Utilities\ProductFilter;

class Product extends Model
{
    protected $guarded = [];

    public function p_sub_category()
    {
        return $this->belongsTo(PSubCategory::class, 'p_sub_category_id');
    }

    public function scopeFilterBy($query, $filters)
    {
        $result = new ProductFilter($query, $filters);
        return $result->applyFilters();
        
    }

}
