<?php

namespace App\Utilities;
use App\Utilities\QueryFilter;

Class BlogFilter extends QueryFilter {
    
    public function sub_category($val) {
        $this->query->where('b_sub_category_id', $val);
    }

}