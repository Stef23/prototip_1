<?php

namespace App\Utilities;
use App\Utilities\QueryFilter;

Class ProductFilter extends QueryFilter {
    
    public function sub_category($val) {
        $this->query->where('p_sub_category_id', $val);
    }

}