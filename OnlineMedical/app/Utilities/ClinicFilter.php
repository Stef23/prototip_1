<?php

namespace App\Utilities;
use App\Utilities\QueryFilter;

Class ClinicFilter extends QueryFilter {
    
    public function category($val) {
        $this->query->where('c_category_id', $val);
    }

}