<?php

namespace App\Utilities;

abstract class QueryFilter {
    protected $query;
    protected $filters;


    public function __construct($query, $filters)
    {
        $this->query = $query;
        $this->filters = $filters;
    }


    public function applyFilters() {
        foreach($this->filters as $filter => $val) {

            if( ! method_exists($this, $filter)) {
                continue;
            }

            if(is_array($val)) {
                if(count($val)) {
                    $this->$filter($val);
                } else {
                    $this->$filter();
                }
            } else {
                if(strlen($val)) {
                    $this->$filter($val);
                } else {
                    $this->$filter();
                }
            }

        }
        
        return $this->query;
    }
}