<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Utilities\BlogFilter;

class Blog extends Model
{
    protected $guarded = [];
    protected $with = ['user'];
    public function user() {
        return $this->belongsTo(User::class);
    }

    public function sub_category() {
        return $this->belongsTo(BSubCategory::class, 'b_sub_category_id');
    }

    public function scopeFilterBy($query, $filters) {
        // dd($filters);
        $result = new BlogFilter($query, $filters);
        return $result->applyFilters();
    }
}
