<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CCategory extends Model
{
    protected $guarded = [];

    public function clinics()
    {
        return $this->hasMany(Clinic::class);
    }
}
