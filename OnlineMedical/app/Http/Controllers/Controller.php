<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    // Custom summernote functions for saving/editing and deleting images localy

    public function saveSummernoteImgLocally($detail) {
        
        
        libxml_use_internal_errors(true);
        $dom = new \domdocument();
        $dom->loadHtml($detail, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
        $images = $dom->getElementsByTagName('img');
        
        foreach ($images as $count => $image) {
           $src = $image->getAttribute('src');
           if (preg_match('/data:image/', $src)) {
               preg_match('/data:image\/(?<mime>.*?)\;/', $src, $groups);
               $mimeType = $groups['mime'];
               $path = '/uploads/images/summernote/' . uniqid('', true) . '.' . $mimeType;
               Storage::disk('public')->put($path, file_get_contents($src));
               $image->removeAttribute('src');
               $image->setAttribute('src', Storage::disk('public')->url($path));
               $image->setAttribute('class', 'img-fluid');
           }
        }
             
        
        return $dom->savehtml();
        
    }

    

    public function deleteSummernoteImgLocally ($detail) {
        libxml_use_internal_errors(true);
        $dom = new \domdocument();
        $dom->loadHtml($detail, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
        $images = $dom->getElementsByTagName('img');

        foreach ($images as $count => $image) {
            $src = $image->getAttribute('src');
            
            $base_url = url('/');

            if ( strpos($src, $base_url) == 0 ) {
                
                $img_path = substr($src, strlen($base_url) + 1);
                
                File::delete($img_path);
            
            }

        }
    }
}
