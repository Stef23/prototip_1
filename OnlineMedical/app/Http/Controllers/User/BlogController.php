<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Blog;
use App\Section;

class BlogController extends Controller
{

    public function index() {
        

        if( ! empty( request()->all() ) ) {
            $blogs = Blog::filterBy( request()->all() )
            ->whereHas('sub_category.category.section', function($q) {
                $q->where('id', 1);
            })->orderBy('created_at', 'desc')->simplePaginate(10);
        } else {
            $blogs = Blog::whereHas('sub_category.category.section', function($q) {
                $q->where('id', 1);
            })->orderBy('created_at', 'desc')->simplePaginate(10);
            
            $blogs->load('sub_category.category.section');
        }

        $categories = Section::find(1)->categories;
        $categories->load('sub_categories');        
        return view('user.pages.blogs.index', compact('blogs', 'categories'));
    }

    public function show(Blog $blog) {
        
        $blog->load('sub_category.blogs');

        $relatedBlogs = $blog->sub_category->blogs()->whereNotIn('id', [$blog->id])->take(2)->get();
        return view('user.pages.blogs.show', compact('blog', 'relatedBlogs'));
    
    }
}
