<?php

namespace App\Http\Controllers\User;

use App\Blog;
use App\Section;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class FitnessController extends Controller
{
    public function index() {

        if( ! empty( request()->all() ) ) {
            $blogs = Blog::filterBy( request()->all() )
            ->whereHas('sub_category.category.section', function($q) {
                $q->where('id', 2);
            })->orderBy('created_at', 'desc')->simplePaginate(10);
        } else {
            $blogs = Blog::whereHas('sub_category.category.section', function($q) {
                $q->where('id', 2);
            })->orderBy('created_at', 'desc')->simplePaginate(10);
            
            $blogs->load('sub_category.category.section');
        }

        $categories = Section::find(2)->categories;
        $categories->load('sub_categories');
        return view('user.pages.fitness.index', compact('blogs', 'categories'));
    }
}
