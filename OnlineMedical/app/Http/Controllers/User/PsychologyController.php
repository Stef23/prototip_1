<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Blog;
use App\Section;
class PsychologyController extends Controller
{
    public function index() {

        if( ! empty( request()->all() ) ) {
            $blogs = Blog::filterBy( request()->all() )
            ->whereHas('sub_category.category.section', function($q) {
                $q->where('id', 3);
            })
            ->orderBy('created_at', 'desc')->simplePaginate(10);
        } else {
            $blogs = Blog::whereHas('sub_category.category.section', function($q) {
                $q->where('id', 3);
            })->orderBy('created_at', 'desc')->simplePaginate(10);
            
            $blogs->load('sub_category.category.section');
        }


        $categories = Section::find(3)->categories;
        $categories->load('sub_categories');
        return view('user.pages.psychology.index', compact('blogs', 'categories'));
    }
}
