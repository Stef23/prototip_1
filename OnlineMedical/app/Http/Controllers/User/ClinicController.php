<?php

namespace App\Http\Controllers\User;

use App\CCategory;
use App\Clinic;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ClinicController extends Controller
{
    public function index() {

        if( ! empty( request()->all() ) ) {
            $clinics = Clinic::filterBy( request()->all() )->orderBy('created_at', 'desc')->simplePaginate(10);
        } else {
            $clinics = Clinic::orderBy('created_at', 'desc')->simplePaginate(10);
        }

        $clinics->load('c_categories');
        $categories = CCategory::all();


        return view('user.pages.clinics.index', compact('clinics', 'categories'));
    }
}
