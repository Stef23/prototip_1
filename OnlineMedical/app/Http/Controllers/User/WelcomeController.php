<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Blog;
use App\Product;

class WelcomeController extends Controller
{
    public function index() {
        $blogs = Blog::orderBy('created_at', 'desc')->take(6)->get();
        $products = Product::orderBy('created_at', 'desc')->take(6)->get();
        
        $latest_fitness_blog = Blog::whereHas('sub_category.category.section', function($q) {
            $q->where('id', 2);
        })->orderBy('created_at', 'desc')->first();

        $latest_psychology_blog = Blog::whereHas('sub_category.category.section', function($q) {
            $q->where('id', 3);
        })->orderBy('created_at', 'desc')->first();


        return view('user.welcome', compact('blogs', 'products', 'latest_fitness_blog', 'latest_psychology_blog'));
    }
}
