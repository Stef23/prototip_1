<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\PCategory;
use App\PSubCategory;
use App\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function index() {
        $categories = PCategory::all();
        $categories->load('sub_categories');
        
        $products = Product::take(3)->get();
        return view('user.pages.products.index', compact('categories', 'products'));
    }

    public function show_sub_category(PSubCategory $sub_category) {
        $sub_category->load('products');
        $products = $sub_category->products;

        return view('user.pages.products.show_sub_category', compact('sub_category', 'products'));
    }
} 
