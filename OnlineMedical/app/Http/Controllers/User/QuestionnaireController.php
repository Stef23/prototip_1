<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Questionnaire;

class QuestionnaireController extends Controller
{
    public function index() {

        $questionnaires = Questionnaire::where('in_production', 1)->get();
        $questionnaires->load('category');

        return view('user.pages.questionnaires.index', ['questionnaires' => $questionnaires]);
    
    }

    public function show(Questionnaire $questionnaire)
    {

        return view('user.pages.questionnaires.show', ['questionnaire' => $questionnaire]);
    }

    // Methods responding to API requests ===========================================
    
    public function get_starter_question(Questionnaire $questionnaire) {
        
        $starter_question = $questionnaire->questions_responses()
            ->where('type', 'question')
            ->where('starter_question', 1)
            ->with('choices', 'questionnaire')
            ->get();
        
        return response()->json($starter_question);
    }

    public function jump_to(Questionnaire $questionnaire, Request $request) {

        $jump_to = $questionnaire->questions_responses()
            ->where('id', $request->next_question_response_id)
            ->with('choices', 'questionnaire')
            ->get();
        
        return response()->json($jump_to);
    }

    public function jump_back(Questionnaire $questionnaire, Request $request) {
        $jump_back = $questionnaire->questions_responses()
            ->where('id', $request->prev_question_response_id)
            ->with('choices', 'questionnaire')
            ->get();
        
        return response()->json($jump_back);
    }

    // public function get_current_question_response(Questionnaire $questionnaire, Request $request) {
    //     $current_question_response = $questionnaire->questions_responses()
    //         ->where('id', $request->current_question_response_id)
    //         ->with('choices', 'questionnaire')
    //         ->get();
        
    //     return response()->json($current_question_response);
    // }
}
