<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\ResponseRequests\ResponseRequest;
use App\Product;
use App\Questionnaire;
use App\QuestionnaireContent;
use Illuminate\Http\Request;
// use Illuminate\Support\Facades\Storage;
// use Illuminate\Support\Facades\File;

use Psy\Util\Str;

class ResponseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }




    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Questionnaire $questionnaire)
    {

        return view('admin.pages.responses.create', compact('questionnaire'));
    }




    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ResponseRequest $request, Questionnaire $questionnaire)
    {
        $data = $request->validated();
        
        $data['questionnaire_contents']['question_response'] = $this->saveSummernoteImgLocally($data['questionnaire_contents']['question_response']);
        

        $success = $questionnaire->questions_responses()->create($data['questionnaire_contents']);

        if($success) {
            return redirect()->back()->withSuccess('Response created successfuly.');
        }

        return redirect()->back()->withSuccess('Something went wrong, Please try again.');

    }




    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(QuestionnaireContent $response)
    {
        return view('admin.pages.responses.edit', compact('response'));
    }




    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ResponseRequest $request, QuestionnaireContent $response)
    {
        $data = $request->validated();

        $data['questionnaire_contents']['question_response'] = $this->saveSummernoteImgLocally($data['questionnaire_contents']['question_response']);

        $success = $response->update($data['questionnaire_contents']);

        if($success) {
            return redirect()->route('show_questionnaire' ,$response->questionnaire->id);
        }

        return redirect()->back()->withSuccess('Something went wrong, Please try again.');
    }




    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(QuestionnaireContent $response)
    {
        $this->deleteSummernoteImgLocally($response['question_response']);

        if($response->delete()) {
            return redirect()->back()->withSuccess('Response deleted successfyly.');
        }
        
        return redirect()->back()->withErrors('Somthing went wrong, please try again.');
    }

    // private function saveSummernoteImgLocally($data) {
        
    //     $detail = $data['questionnaire_contents']['question_response'];
    //     libxml_use_internal_errors(true);
    //     $dom = new \domdocument();
    //     $dom->loadHtml($detail, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
    //     $images = $dom->getElementsByTagName('img');
        
    //     foreach ($images as $count => $image) {
    //        $src = $image->getAttribute('src');
    //        if (preg_match('/data:image/', $src)) {
    //            preg_match('/data:image\/(?<mime>.*?)\;/', $src, $groups);
    //            $mimeType = $groups['mime'];
    //            $path = '/uploads/images/responses/' . uniqid('', true) . '.' . $mimeType;
    //            Storage::disk('public')->put($path, file_get_contents($src));
    //            $image->removeAttribute('src');
    //            $image->setAttribute('src', Storage::disk('public')->url($path));
    //        }
    //     }
             
        
    //     return $dom->savehtml();
        
    // }

    // private function deleteSummernoteImgLocally ($data) {
    //     $detail = $data['question_response'];
    //     libxml_use_internal_errors(true);
    //     $dom = new \domdocument();
    //     $dom->loadHtml($detail, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
    //     $images = $dom->getElementsByTagName('img');

    //     foreach ($images as $count => $image) {
    //         $src = $image->getAttribute('src');
            
    //         $base_url = url('/');

    //         if ( strpos($src, $base_url) == 0 ) {
                
    //             $img_path = substr($src, strlen($base_url) + 1);
                
    //             File::delete($img_path);
            
    //         }

    //     }
    // }
}
