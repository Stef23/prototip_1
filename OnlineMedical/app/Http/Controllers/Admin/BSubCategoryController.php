<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\BSubCategory;
use App\BCategory;
use Illuminate\Http\Request;
use App\Http\Requests\BlogRequests\BSubCategoryRequest;

class BSubCategoryController extends Controller
{

    public function create(Request $request) {
        $category = BCategory::find($request->category);
        
        if(!$category) {
            return redirect()->route('home')->withErrors(['Category' => ['Please choose a main category before preceding!']]);
        }

        return view('admin.pages.blogs.sub_categories.create', ['category' => $category]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BCategory $category, BSubCategoryRequest $request)
    {
        $data = $request->validated();

        if($category->sub_categories()->create($data)) {
            return redirect()->back()->withSuccess('Category created successfuly.');
        }

        return redirect()->back()->withErrors('Somthing went wrong, Please try again.');
    }


    public function destroy(BSubCategory $sub_category)
    {
        if(count($sub_category->blogs)) {
            return redirect()->back()->withErrors(['sub_category' => ['Please reasign all blogs under this sub_category to another one, before deleting it!']]);
        }

        if($sub_category->delete()) {
            return redirect()->back()->withSuccess('sub_ategory deleted successfuly.');
        }

        return redirect()->back()->withErrors('Somthing went wrong, Please try again.');
   
    }

    public function edit(BSubCategory $sub_category)
    {
        return view('admin.pages.blogs.sub_categories.edit', compact('sub_category'));
    }



    public function update(BSubCategoryRequest $request, BSubCategory $sub_category)
    {
        $data = $request->validated();

        if($sub_category->update($data)) {
            return redirect()->route('show_all_blog_categories');
        }
        
        return redirect()->back()->withErrors('Somthing went wrong, please try again.');
    }
}
