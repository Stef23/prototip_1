<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\QuestionnaireRequests\QuestionnaireRequest;
use Illuminate\Http\Request;
use App\Questionnaire;
use App\QCategory;
use App\QuestionChoice;
use App\QuestionnaireContent;

class QuestionnaireController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $questionnaires = Questionnaire::orderByDesc('in_production')
                                        ->orderByDesc('created_at')
                                        ->paginate(10);
                                        
        $questionnaires->load('category.icon');

        return view('admin.pages.questionnaires.index', compact('questionnaires'));
    }




    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = QCategory::all();

        return view('admin.pages.questionnaires.create', compact('categories'));
    }




    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(QuestionnaireRequest $request)
    {
        $data = $request->validated();

        $questionnaire = auth()->user()->questionnaires()->create($data);

        if($questionnaire) {
            return redirect()->route('show_questionnaire', $questionnaire->id);
        }

        return redirect()->back()->withErrors('Somthing went wrong, please try again.');
    }




    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Questionnaire $questionnaire)
    {
        $questionnaire->load('questions_responses.choices');
        
        $questions = $questionnaire->questions_responses()->where('type', 'question')->get();
        $responses = $questionnaire->questions_responses()->where('type', 'response')->get();
        
        $view_data = [
            'questionnaire' => $questionnaire, 
            'questions' => $questions, 
            'responses' => $responses
        ];

        return view('admin.pages.questionnaires.show', $view_data);
    }




    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Questionnaire $questionnaire)
    {
        $categories = QCategory::all();

        return view('admin.pages.questionnaires.edit', compact('questionnaire', 'categories'));
    }




    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(QuestionnaireRequest $request, Questionnaire $questionnaire)
    {
        $data = $request->validated();

        if($questionnaire->update($data)) {
            return redirect()->route('show_questionnaire', $questionnaire->id);
        }

        return redirect()->back()->withErrors('Something went wrong, please try again.');
    }


    public function set_in_production(Questionnaire $questionnaire) {
        
        $questionnaire->load('questions_responses.choices');
        $questions = $questionnaire->questions_responses->where('type', 'question');


        if(count($questions)) {
            
            $choices_have_jump_to = [];
            
            foreach($questions as $question) {
                if( count($question->choices->whereNotNull('jump_to')) ) {
                    $choices_have_jump_to['true'][] = true;
                } else {
                    $choices_have_jump_to['false'][] = false;
                }
            }

            if( !( isset($choices_have_jump_to['false']) && count($choices_have_jump_to) > 0 ) ) {

                if($questionnaire->in_production) {
                    $questionnaire->update(['in_production' => 0]);
                    return redirect()->back()->withSuccess('The Questionnaire has been successfully withdrawn from production.');
                } else {
                    $questionnaire->update(['in_production' => 1]);
                    return redirect()->back()->withSuccess("The Questionnaire has been successfully published.");
                }

            } else {

                return redirect()->back()->withErrors("Please please make your questionnaire schema before you publish");
            }

        }

        return redirect()->back()->withErrors("Please add questions before you publish your questionnaire");        


    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Questionnaire $questionnaire)
    {
        if($questionnaire->delete()) {
            return redirect()->route('home');
        }
        
        return redirect()->back()->withErrors('Somthing went wrong, please try again.');

        // // abort(404);
    }




    public function make_questionnaire_schema(Questionnaire $questionnaire, Request $request)
    {
        if( ! isset($request['question_choices'])) {
            return redirect()->back()->withErrors('You must provide choice for the question !');
        }

        if( ! isset($request['starter_question_id'])) {
            $request->flash();
            return redirect()->back()->withErrors('You must select a starter question for your questionnaire !');
        }

        foreach ($request['question_choices'] as $choice_id => $jump_to) {            
            QuestionChoice::where('id', $choice_id)->update($jump_to);
        }

        QuestionnaireContent::whereIn('id', $request['starter_question_id'])
            ->where('questionnaire_id', $questionnaire->id)
            ->update(['starter_question' => 1]);
        
        QuestionnaireContent::whereNotIn('id', $request['starter_question_id'])
            ->where('questionnaire_id', $questionnaire->id)
            ->update(['starter_question' => 0]);

        return redirect()->back()->withSuccess('The Questionnaire schema has been successfully saved.');
    }




    public function preview(Questionnaire $questionnaire)
    {

        $questionnaire->load('questions_responses.choices');
        $questions = $questionnaire->questions_responses->where('type', 'question');


        if(count($questions)) {
            
            $choices_have_jump_to = [];
            
            foreach($questions as $question) {
                if( count($question->choices->whereNotNull('jump_to')) ) {
                    $choices_have_jump_to['true'][] = true;
                } else {
                    $choices_have_jump_to['false'][] = false;
                }
            }

            if( !( isset($choices_have_jump_to['false']) && count($choices_have_jump_to) > 0 ) ) {

                return view('admin.pages.questionnaires.preview', ['questionnaire' => $questionnaire]);

            } else {

                return redirect()->back()->withErrors(['Choices' => ["Please please make your questionnaire schema if you whant to preview the questionnaire"]]);

            }

        }

        return redirect()->back()->withErrors(['Questions' => ["Please add questions if you whant to preview the questionnaire"]]);

        
    }


    

    // Methods responding to API requests ===========================================
    
    public function get_starter_question(Questionnaire $questionnaire) {
        
        $starter_question = $questionnaire->questions_responses()
            ->where('type', 'question')
            ->where('starter_question', 1)
            ->with('choices', 'questionnaire')
            ->get();
        
        return response()->json($starter_question);
    }

    public function jump_to(Questionnaire $questionnaire, Request $request) {

        $jump_to = $questionnaire->questions_responses()
            ->where('id', $request->next_question_response_id)
            ->with('choices', 'questionnaire')
            ->get();
        
        return response()->json($jump_to);
    }

    public function jump_back(Questionnaire $questionnaire, Request $request) {
        $jump_back = $questionnaire->questions_responses()
            ->where('id', $request->prev_question_response_id)
            ->with('choices', 'questionnaire')
            ->get();
        
        return response()->json($jump_back);
    }
    
}
