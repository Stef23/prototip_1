<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProductRequests\ProductRequest;
use App\PCategory;
use App\PSubCategory;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if( ! empty( request()->all() ) ) {
            $products = Product::filterBy( request()->all() )
                                ->orderByDesc('promoted')->get();
        } else {
            $products = Product::orderByDesc('promoted')->get();        
        }

        $products->load('p_sub_category');            

        $categories = PCategory::all();
        $categories->load('sub_categories');
        
        return view('admin.pages.products.index', compact('products', 'categories'));
    }




    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $categories = PCategory::find($request->category);
        
        if(!$categories) {
            return redirect()->route('home')->withErrors(['Category' => ['Please choose a main category before preceding!']]);
        }

        $sub_categories = $categories->sub_categories;

        return view('admin.pages.products.create', compact('sub_categories'));
    }




    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductRequest $request)
    {
        $data = $request->validated();
        
        $data['image'] = $request->image->store('uploads/images/products', 'public');
        
        if(Product::create($data)) {
            return redirect()->route('show_all_products');
        }

        return redirect()->back()->withErrors('Somthing went wrong, please try again.');
    }




    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        $product->load('p_sub_category.p_category.sub_categories');
        
        $sub_categories = $product->p_sub_category->p_category->sub_categories;
        
        return view('admin.pages.products.edit', compact('product','sub_categories'));
    }




    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProductRequest $request, Product $product)
    {
        $data = $request->validated();
        
        if($request->hasFile('image')){
            $data['image'] = $request->image->store('uploads/images/products', 'public');
        }

        if($product->update($data)) {
            return redirect()->route('show_all_products');
        }

        return redirect()->back()->withErrors('Somthing went wrong, please try again.');
    }




    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        File::delete("storage/".$product->image);

        if($product->delete()) {
            return redirect()->back()->withSuccess('Product deleted successfuly.');
        }
        
        return redirect()->back()->withErrors('Somthing went wrong, please try again.');

    }



    public function set_product_promotion_status(Product $product)
    {
        if($product->promoted) {
            $product->update(['promoted' => 0]);
        } else {
            $product->update(['promoted' => 1]);
        }

        return redirect()->back();
    }


    
}
