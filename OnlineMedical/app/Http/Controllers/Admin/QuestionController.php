<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\QuestionRequests\QuestionRequest;
use Illuminate\Http\Request;
use App\Questionnaire;
use App\QuestionnaireContent;
use App\QuestionChoice;

class QuestionController extends Controller
{
    public function create(Questionnaire $questionnaire)
    {
        $questionnaire->load('questions_responses.choices');

        return view('admin.pages.questions.create', compact('questionnaire'));
    }




    public function store(QuestionRequest $request, Questionnaire $questionnaire)
    {
        $data = $request->validated();

        $questionnaire_content = $questionnaire->questions_responses()->create($data['questionnaire_contents']);
        
        $questionnaire_content->choices()->createMany($data['choices']);

        if($questionnaire_content) {
            return redirect()->route('show_questionnaire', $questionnaire->id);
        }

        return redirect()->back()->withErrors('Something went wrong, please try again.');
    }


    

    public function edit(QuestionnaireContent $question)
    {
        $question->load('questionnaire', 'choices');

        return view('admin.pages.questions.edit', compact('question'));
    }




    public function update(QuestionRequest $request, QuestionnaireContent $question)
    {

        if( ! isset($request['choices']) ) {
            return redirect()->back()->withErrors(['Choices' => ["You must have at least one choice!"]]);        
        }
        
        $data = $request->validated();


        $deletedChoicesIds = [];

        $question->update($data['questionnaire_contents']);

        foreach($data['choices'] as $choice) {

            if(isset($choice['id'])) {
                $deletedChoicesIds[] = $choice['id'];
            }

        };

        
        $question->choices()->whereNotIn('id', $deletedChoicesIds)->delete();
                    

        foreach($data['choices'] as $choice) {

            if(isset($choice['id'])) {

                $question->choices()
                    ->where('id', $choice['id'])
                    ->update(['choice' => $choice['choice']]);

            } else {

                $question->choices()->create([
                    'choice' => $choice['choice']
                ]);
            }

        };  

        return redirect()->route('show_questionnaire', $question->questionnaire->id);
    }




    public function destroy(QuestionnaireContent $question)
    {
        if($question->delete()) {
            return redirect()->back()->withSuccess('Question deleted successfuly.');
        }
        
        return redirect()->back()->withErrors('Somthing went wrong, please try again.');

        // abort(404);
    }
}
