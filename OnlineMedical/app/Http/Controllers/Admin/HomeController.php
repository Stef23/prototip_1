<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\QCategory;
use App\Questionnaire;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }




    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $categories = QCategory::all();
        $categories->load('icon');

        $allQuestionnaires = Questionnaire::orderByDesc('created_at')
                                            ->where('in_production', 1)
                                            ->limit(3)
                                            ->get();
                                            
        $allQuestionnaires->load('category');
        
        return view('admin.home', compact('allQuestionnaires', 'categories'));
    }


    
}
