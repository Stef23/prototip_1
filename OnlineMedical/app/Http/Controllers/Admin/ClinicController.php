<?php

namespace App\Http\Controllers\Admin;

use App\CCategory;
use App\Clinic;
use App\Http\Controllers\Controller;
use App\Http\Requests\ClinicRequests\ClinicRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class ClinicController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clinics = Clinic::all();
        $clinics->load('c_categories');

        return view('admin.pages.clinics.index', compact('clinics'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = CCategory::all();

        return view('admin.pages.clinics.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ClinicRequest $request)
    {
        $data = $request->validated();

        $data['image'] = $request->image->store('uploads/images/clinics', 'public');

        if(Clinic::create($data)) {
            return redirect()->route('show_all_clinics');
        }
        
        return redirect()->back()->withErrors('Somthing went wrong, please try again.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Clinic $clinic)
    {
        $categories = CCategory::all();

        return view('admin.pages.clinics.edit', compact('clinic', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ClinicRequest $request, Clinic $clinic)
    {
        $data = $request->validated();

        if($request->hasFile('image')) {
            $data['image'] = $request->image->store('uploads/images/clinics', 'public');
        }

        if($clinic->update($data)) {
            return redirect()->route('show_all_clinics');
        }
        
        return redirect()->back()->withErrors('Somthing went wrong, please try again.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Clinic $clinic)
    {
        File::delete("storage/".$clinic->image);

        if($clinic->delete()) {
            return redirect()->back()->withSuccess('Clinic deleted successfuly.');
        }
        
        return redirect()->back()->withErrors('Somthing went wrong, please try again.');

        // abort(404);
    }
}
