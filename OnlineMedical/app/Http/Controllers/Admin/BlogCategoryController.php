<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\BCategory;
use App\Http\Requests\BlogRequests\BlogCategoryRequest;
use App\Section;

class BlogCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = BCategory::withCount('sub_categories')
                                ->orderBy('created_at', 'desc')
                                ->get();
                                
        return view('admin.pages.blogs.categories.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $sections = Section::all();
        return view('admin.pages.blogs.categories.create', compact('sections'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BlogCategoryRequest $request)
    {
        $data = $request->validated();

        if(BCategory::create($data)) {
            return redirect()->back()->withSuccess('Category created successfuly.');
        }

        return redirect()->back()->withErrors('Somthing went wrong, Please try again.');
    }


    public function destroy(BCategory $category)
    {
        if(count($category->blogs)) {
            return redirect()->back()->withErrors(['sub_category' => ['Please reasign all blogs under this category to another one, before deleting it!']]);
        }

        if($category->delete()) {
            return redirect()->back()->withSuccess('Category deleted successfuly.');
        }

        return redirect()->back()->withErrors('Somthing went wrong, Please try again.');

        
    }

    public function get_main_category($for) {
        if($for == 'blog') {
            $route = route('create_blog');

        } else if($for == 'sub_category') {
            $route = route('create_blog_sub_category');
        }
        
        $categories = BCategory::all();
        
        return view('admin.pages.blogs.sub_categories.get_main_category', ['categories' => $categories, 'route' => $route]);
    }

}
