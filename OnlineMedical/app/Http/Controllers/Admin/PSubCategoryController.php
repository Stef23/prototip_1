<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProductRequests\ProductSubCategoryRequest;
use Illuminate\Http\Request;
use App\PCategory;
use App\PSubCategory;
class PSubCategoryController extends Controller
{


    public function create(Request $request) {
        
        $category = PCategory::find($request->category);

        if(!$category) {
            return redirect()->route('home')->withErrors(['Category' => ['Please choose a main category before preceding!']]);
        }

        return view('admin.pages.products.sub_categories.create', ['category' => $category]);
    }



    public function store(PCategory $category, ProductSubCategoryRequest $request) {

        $data = $request->validated();

        if($category->sub_categories()->create($data)) {
            return redirect()->back()->withSuccess('Sub category created successfuly.');
        }
        
        return redirect()->back()->withErrors('Somthing went wrong, please try again.');
    }



    public function destroy(PSubCategory $sub_category) {

        if(count($sub_category->products)) {
            return redirect()->back()->withErrors(['sub_category' => ['Please reasign all products under this sub-category to another one, before deleting it!']]);
        }

        if($sub_category->delete()) {
            return redirect()->back()->withSuccess('Sub Category deleted successfuly');
        }

        return redirect()->back()->withErrors('Somthing went wrong, please try again.');

        // abort(404);
    }



    public function edit(PSubCategory $sub_category)
    {
        return view('admin.pages.products.sub_categories.edit', compact('sub_category'));
    }



    public function update(ProductSubCategoryRequest $request, PSubCategory $sub_category)
    {
        $data = $request->validated();

        if($sub_category->update($data)) {
            return redirect()->route('show_all_product_categories');
        }
        
        return redirect()->back()->withErrors('Somthing went wrong, please try again.');
    }
}
