<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\QuestionnaireRequests\QuestionnaireCategoryRequest;
use App\Icon;
use App\QCategory;
use Illuminate\Http\Request;

class QuestionnaireCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = QCategory::withCount('questionnaires')->orderByDesc('created_at')->get();
        $categories->load('icon');

        return view('admin.pages.questionnaires.categories.index', compact('categories'));
    }




    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $icons = Icon::all();

        return view('admin.pages.questionnaires.categories.create', compact('icons'));
    }




    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(QuestionnaireCategoryRequest $request)
    {
        $data = $request->validated();

        if(QCategory::create($data)) {
            return redirect()->back()->withSuccess('Category created successfuly.');
        }

        return redirect()->back()->withErrors('Something went wrong, please try again.');
    }




    public function edit(QCategory $category)
    {
        $icons = Icon::all();

        return view('admin.pages.questionnaires.categories.edit', compact('category', 'icons'));
    }




    public function update(QuestionnaireCategoryRequest $request, QCategory $category)
    {
        $data = $request->validated();

        if($category->update($data)) {
            return redirect()->route('show_all_questionnaire_categories');
        }

        return redirect()->back()->withErrors('Something went wrong, please try again.');
    }




    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(QCategory $category)
    {
        $category->load('questionnaires');
        
        if(count($category->questionnaires)) {
            return redirect()->back()->withErrors(['Category' => ['Please reasign all questionnaires under this category to another one, before deleting it!']]);
        }
        
        if($category->delete()) {
            return redirect()->back()->withSuccess('Category deleted successfuly.');
        }

        return redirect()->back()->withErrors('Something went wrong, please try again.');

        // abort(404);
    }
}
