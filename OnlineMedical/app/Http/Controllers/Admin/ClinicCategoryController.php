<?php

namespace App\Http\Controllers\Admin;

use App\CCategory;
use App\Http\Controllers\Controller;
use App\Http\Requests\ClinicRequests\ClinicCategoryRequest;
use Illuminate\Http\Request;

class ClinicCategoryController extends Controller
{
    public function index()
    {
        $categories = CCategory::withCount('clinics')->get();

        return view('admin.pages.clinics.categories.index', compact('categories'));
    }



    public function create()
    {
        return view('admin.pages.clinics.categories.create');
    }



    public function store(ClinicCategoryRequest $request, CCategory $category)
    {
        $data = $request->validated();
        
        if($category->create($data)) {
            return redirect()->back()->withSuccess('Category created successfuly.');
        }

        return redirect()->back()->withErrors('Something went wrong, please try again.');
    }



    public function edit(CCategory $category)
    {
        return view('admin.pages.clinics.categories.edit', compact('category'));
    }



    public function update(ClinicCategoryRequest $request, CCategory $category)
    {
        $data = $request->validated();

        $category->update($data);

        return redirect()->route('show_all_clinic_categories');
    }



    public function destroy(CCategory $category)
    {
        $category->load('clinics');
        
        if(count($category->clinics)) {
            return redirect()->back()->withErrors(['Category' => ['Please reasign all clinics under this category to another one, before deleting it!']]);
        }

        if($category->delete()) {
            return redirect()->back()->withSuccess('Category deleted successfuly.');
        }

        return redirect()->back()->withErrors('Something went wrong, please try again.');

        // abort(404);
        

    }
}
