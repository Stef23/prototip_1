<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProductRequests\ProductCategoryRequest;
use App\PCategory;
use Illuminate\Http\Request;

class ProductCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = PCategory::withCount('sub_categories')->orderBy('created_at', 'desc')->with('sub_categories')->get();
        
        return view('admin.pages.products.categories.index', compact('categories'));
    }




    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = PCategory::all();

        return view('admin.pages.products.categories.create', compact('categories'));
    }




    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductCategoryRequest $request, PCategory $category)
    {
        $data = $request->validated();

        if($category->create($data)) {
            return redirect()->back()->withSuccess('Category created successfuly.');
        }

        return redirect()->back()->withErrors('Something went wrong, please try again.');

    }


    

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(PCategory $category)
    {
        return view('admin.pages.products.categories.edit', compact('category'));
    }




    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProductCategoryRequest $request, PCategory $category)
    {
        $data = $request->validated();

        if($category->update($data)) {
            return redirect()->route('show_all_product_categories');
        }

        return redirect()->back()->withErrors('Something went wrong, please try again.');
    }




    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(PCategory $category)
    {

        $category->load('sub_categories');
        
        if(count($category->sub_categories)) {
            return redirect()->back()->withErrors(['Category' => ['Please reasign all sub_categories under this category to another one, before deleting it!']]);
        }

        if($category->delete()) {
            return redirect()->back()->withSuccess('Category deleted successfuly');
        }

        return redirect()->back()->withErrors('Something went wrong, please try again.');

        // abort(404);
    }




    public function get_main_category($for) {
        if($for == 'product') {
            $route = route('create_product');

        } else if($for == 'sub_category') {
            $route = route('create_product_sub_category');
        }
        
        $categories = PCategory::all();
        
        return view('admin.pages.products.sub_categories.get_main_category', ['categories' => $categories, 'route' => $route]);
    }

    
}
