<?php

namespace App\Http\Controllers\Admin;

use App\BCategory;
use App\BSubCategory;
use App\Blog;
use App\Http\Controllers\Controller;
use App\Http\Requests\BlogRequests\BlogRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = BCategory::all();
        
        $blogs = Blog::orderBy('created_at', 'desc')->with('sub_category')->get();

        return view('admin.pages.blogs.index', compact('blogs', 'categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

        $categories = BCategory::find($request->category);
        
        if(!$categories) {
            return redirect()->back()->withErrors(['Category' => ['Please choose a main category before preceding!']]);
        }

        $sub_categories = $categories->sub_categories;

        return view('admin.pages.blogs.create', compact('sub_categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BlogRequest $request)
    {
        $data = $request->validated();
        // dd($data);
        $data['cover_image'] = $request->cover_image->store('uploads/images/blogs', 'public');
        
        $data['body'] = $this->saveSummernoteImgLocally($data['body']);

        $success = auth()->user()->blogs()->create($data);

        if($success) {
            return redirect()->route('show_all_blogs');
        }
        
        return redirect()->back()->withErrors('Something went wrong, please try again.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Blog $blog)
    {
        return view('admin.pages.blogs.show', compact('blog'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Blog $blog)
    {
        $sub_categories = BSubCategory::all();
        return view('admin.pages.blogs.edit', compact('blog', 'sub_categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(BlogRequest $request, Blog $blog)
    {
        $data = $request->validated();

        $data['body'] = $this->saveSummernoteImgLocally($data['body']);

        if($request->hasFile('cover_image')) {
            $data['cover_image'] = $request->cover_image->store('uploads/images/blogs', 'public');
        }

        if($blog->update($data)) {
            return redirect()->route('show_all_blogs');
        }
        
        return redirect()->back()->withErrors('Something went wrong, please try again.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Blog $blog)
    {
        File::delete("storage/".$blog->cover_image);
        $this->deleteSummernoteImgLocally($blog->body);
        if($blog->delete()) {
            return redirect()->back()->withSuccess('Blog deleted successfuly.');
        }
        
        return redirect()->back()->withErrors('Somthing went wrong, please try again.');

    }
}
