<?php

namespace App\Http\Requests\ResponseRequests;

use Illuminate\Foundation\Http\FormRequest;

class ResponseRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'questionnaire_contents.question_response' => 'required',
            'questionnaire_contents.type' => 'required',
            // 'files' => 'sometimes'
        ];

        return $rules;
    }

    public function messages()
    {
        return [
            'questionnaire_contents.question_response.required' => 'You must provide a response !',
            'questionnaire_contents.type.required' => 'Response type is required !',
        ];
    }
}
