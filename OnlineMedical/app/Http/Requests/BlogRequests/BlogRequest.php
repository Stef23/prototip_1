<?php

namespace App\Http\Requests\BlogRequests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Request;

class BlogRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $routeName = Request::route()->getName();

        $rules = [
            'title' => 'required', 
            'body' => 'required',
            'blog_tags' => 'required|max:255',
            'b_sub_category_id' => 'required',
        ];


        if($routeName == "store_blog") {

            $rules['cover_image'] = 'required|file|image|dimensions:min_width=750,min_height=400';
        } 
        else {

            $rules['cover_image'] = 'sometimes|file|image|dimensions:min_width=750,min_height=400';
        }

        return $rules;
    }


    public function messages()
    {
        return [
            'title.required'            => 'You must provide a title for the blog !',
            'body.required'             => 'You must provide a body for the blog !',
            'cover_image.required'      => 'You must provide an image for the blog !',
            'cover_image.file'          => 'The cover image must be of the FILE type !',
            'cover_image.image'         => 'The cover image must be of the IMAGE type !',
            'cover_image.dimensions'    => 'The cover image has invalid dimensions. Minimal dimensions: width:750px, height:400px  !',
            'blog_tags.required'        => 'You must provide tags for the blog !',
            'blog_tags.max'             => 'The tags may not be greater than 255 characters !',
            'b_sub_category_id.required'    => 'You must choose category for your blog !',
            
        ];
    }
}
