<?php

namespace App\Http\Requests\BlogRequests;

use Illuminate\Foundation\Http\FormRequest;

class BlogCategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'category' => 'required',
            'section_id' => 'required',
        ];

        return $rules;
    }

    public function messages()
    {
        return [
            'category.required' => 'You must provide a name for the category !',
            'section_id.required' => 'You must choose a section under which your category will be stored !',
        ];
    }
}
