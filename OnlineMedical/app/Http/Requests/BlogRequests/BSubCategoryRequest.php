<?php

namespace App\Http\Requests\BlogRequests;

use Illuminate\Foundation\Http\FormRequest;

class BSubCategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'sub_category' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'sub_category.required' => 'You must provide a name for the sub_category !',
        ];
    }
}
