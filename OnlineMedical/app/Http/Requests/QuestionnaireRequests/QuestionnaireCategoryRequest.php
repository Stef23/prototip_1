<?php

namespace App\Http\Requests\QuestionnaireRequests;

use Illuminate\Foundation\Http\FormRequest;

class QuestionnaireCategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'category' => 'required',
            'icon_id'  => 'required'
        ];

        return $rules;
    }

    public function messages()
    {
        return [
            'category.required'  => 'You must provide a name for the category !',
            'icon_id.required'  => 'You must choose an icon for your category'
        ];
    }
}
