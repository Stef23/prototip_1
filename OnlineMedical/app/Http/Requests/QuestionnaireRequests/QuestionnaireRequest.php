<?php

namespace App\Http\Requests\QuestionnaireRequests;

use Illuminate\Foundation\Http\FormRequest;

class QuestionnaireRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'q_category_id' => 'required',
            'name'          => 'required',
            'description'   => 'required'
        ];

        return $rules;
    }

    public function messages()
    {
        return [
            'q_category_id.required'    => 'You must choose category for your questionnaire !',
            'name.required'             => 'You must provide a name for the questionnaire !',
            'description.required'      => 'You must provide a description for the questionnaire !'  
        ];
    }
}
