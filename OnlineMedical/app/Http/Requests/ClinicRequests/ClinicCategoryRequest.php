<?php

namespace App\Http\Requests\ClinicRequests;

use Illuminate\Foundation\Http\FormRequest;

class ClinicCategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'category' => 'required'
        ];

        return $rules;
    }

    public function messages()
    {
        return [
            'category.required' => 'You must provide a name for the category !'
        ];
    }
}
