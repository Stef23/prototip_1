<?php

namespace App\Http\Requests\ClinicRequests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Request;

class ClinicRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $routeName = Request::route()->getName();

        $rules = [
            'name'              => 'required',
            'description'       => 'required',
            'location'          => 'required',
            'telephone_number'  => 'required',
            'c_category_id'     => 'required',
        ];

        if($routeName == "store_clinic") {

            $rules['image'] = 'required|file|image|dimensions:min_width=750,min_height=400';
        } 
        else {

            $rules['image'] = 'sometimes|file|image|dimensions:min_width=750,min_height=400';
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'name.required'             => 'You must provide a name for the clinic !',
            'description.required'      => 'You must provide a description for the clinic !',
            'location.required'         => 'You must provide a location for the clinic !',
            'image.required'            => 'You must provide an image for the clinic !',
            'image.file'                => 'The image must be of the FILE type !',
            'image.image'               => 'The image must be of the IMAGE type !',
            'image.dimensions'          => 'The image has invalid dimensions. Minimal dimensions: width:750px, height:400px !',
            'telephone_number.required' => 'You must provide telephone number for the clinic !',
            'telephone_number.integer'  => 'The telephone number must be of the INTEGER type !',
            'c_category_id.required'    => 'You must create category for your clinic !',
            
        ];
    }
}
