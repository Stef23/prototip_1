<?php

namespace App\Http\Requests\QuestionRequests;

use Illuminate\Foundation\Http\FormRequest;

class QuestionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'questionnaire_contents.question_response'  => 'required',
            'questionnaire_contents.type'               => 'required',
            'choices.*.choice'                          => 'required'
        ];

        return $rules;
    }

    public function messages()
    {
        return [
            'questionnaire_contents.question_response.required' => 'You must provide a question title !',
            'questionnaire_contents.type.required'              => 'Question type is required !',
            'choices.*.choice.required'                         => 'A choice must be provided !'
        ];
    }
}
