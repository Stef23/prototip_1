<?php

namespace App\Http\Requests\ProductRequests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Request;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $routeName = Request::route()->getName();

        $rules = [
            'name'              => 'required', 
            'short_description' => 'required', 
            'description'       => 'required',
            'p_sub_category_id' => 'required',
        ];


        if($routeName == "store_product") {

            $rules['image'] = 'required|file|image|dimensions:min_width=750,min_height=400';
        } 
        else {

            $rules['image'] = 'sometimes|file|image|dimensions:min_width=750,min_height=400';
        }

        return $rules;
    }


    public function messages()
    {
        return [
            'name.required'                 => 'You must provide a name for the product !',
            'short_description.required'    => 'You must provide a short_description for the product !',
            'description.required'          => 'You must provide description for the product !',
            'image.required'                => 'You must provide an image for the product !',
            'image.file'                    => 'The image must be of the FILE type !',
            'image.image'                   => 'The image must be of the IMAGE type !',
            'image.dimensions'              => 'The image has invalid dimensions. Minimal dimensions: width:750px, height:400px  !',
            'p_sub_category_id.required'    => 'You must choose sub_category for your product !',
            
        ];
    }
}
