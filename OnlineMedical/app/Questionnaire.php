<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Questionnaire extends Model
{
    protected $guarded = [];

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function questions_responses() {
        return $this->hasMany(QuestionnaireContent::class);
    }

    public function category() {
        return $this->belongsTo(QCategory::class, 'q_category_id');
    }

}
