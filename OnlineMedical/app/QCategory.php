<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Icon;

class QCategory extends Model
{
    protected $guarded = [];
    // public $with = ['']
    public function icon() {
        return $this->belongsTo(Icon::class);
    }

    public function questionnaires() {
        return $this->hasMany(Questionnaire::class);
    }
}
