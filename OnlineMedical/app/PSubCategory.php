<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
class PSubCategory extends Model
{
    protected $guarded = [];
    protected $with = ['products', 'p_category'];

    public function p_category() {
        return $this->belongsTo(PCategory::class);
    }

    public function products() {
        return $this->hasMany(Product::class);
    }
}
