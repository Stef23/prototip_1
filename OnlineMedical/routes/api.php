<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::namespace('User')->group(function() {
    Route::get('/questionnaires/{questionnaire}', 'QuestionnaireController@get_starter_question');
    Route::get('/questionnaires/{questionnaire}/jump-to', 'QuestionnaireController@jump_to');
    Route::get('/questionnaires/{questionnaire}/jump-back', 'QuestionnaireController@jump_back');
    // Route::get('/questionnaires/{questionnaire}/get-current', 'QuestionnaireController@get_current_question_response');
});

Route::namespace('Admin')->group(function() {
    Route::get('/questionnaires/preview/{questionnaire}', 'QuestionnaireController@get_starter_question');
    Route::get('/questionnaires/preview/{questionnaire}/jump-to', 'QuestionnaireController@jump_to');
    Route::get('/questionnaires/preview/{questionnaire}/jump-back', 'QuestionnaireController@jump_back');
});