<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();




Route::namespace('Admin')->prefix('home-admin')->middleware('auth')->group(function() {
    
    Route::get('/', 'HomeController@index')->name('home');

    Route::get('/questionnaires',                                   'QuestionnaireController@index')->name('show_all_questionnaires');
    Route::get('/questionnaires/create',                            'QuestionnaireController@create')->name('create_questionnaire');
    Route::get('/questionnaires/{questionnaire}/show',              'QuestionnaireController@show')->name('show_questionnaire');
    Route::get('/questionnaires/{questionnaire}/preview',           'QuestionnaireController@preview')->name('preview_questionnaire');
    Route::get('/questionnaires/{questionnaire}/set_in_production', 'QuestionnaireController@set_in_production')->name('set_questionnaire_in_production');

    Route::get('/questionnaires/{questionnaire}/edit',              'QuestionnaireController@edit')->name('edit_questionnaire');
    Route::get('/questionnaires/{questionnaire}/delete',            'QuestionnaireController@destroy')->name('delete_questionnaire');
    Route::post('/questionnaires/{questionnaire}/update',           'QuestionnaireController@update')->name('update_questionnaire');
    Route::post('/questionnaires/store',                            'QuestionnaireController@store')->name('store_questionnaire');
    Route::post('/questionnaires/{questionnaire}/question/schema',  'QuestionnaireController@make_questionnaire_schema')->name('make_questionnaire_schema');
    
    
    Route::get('/questionnaires/{questionnaire}/question/create',   'QuestionController@create')->name('create_question');
    Route::get('/questionnaires/questionnaire/{question}/edit',     'QuestionController@edit')->name('edit_question');
    Route::get('/questionnaires/questionnaire/{question}/delete',   'QuestionController@destroy')->name('delete_question');
    Route::post('/questionnaires/questionnaire/{question}/update',  'QuestionController@update')->name('update_question');
    Route::post('/questionnaires/{questionnaire}/question/store',   'QuestionController@store')->name('store_question');
    

    Route::get('/categories',                       'QuestionnaireCategoryController@index')->name('show_all_questionnaire_categories');
    Route::get('/categories/create',                'QuestionnaireCategoryController@create')->name('create_questionnaire_category');
    Route::get('/categories/{category}/delete',     'QuestionnaireCategoryController@destroy')->name('delete_questionnaire_category');
    Route::get('/categories/{category}/edit',       'QuestionnaireCategoryController@edit')->name('edit_questionnaire_category');
    Route::post('/categories/{category}/update',    'QuestionnaireCategoryController@update')->name('update_questionnaire_category');
    Route::post('/categories/store',                'QuestionnaireCategoryController@store')->name('store_questionnaire_category');


    Route::get('questionnaires/{response}/response/edit',           'ResponseController@edit')->name('edit_response');
    Route::get('questionnaires/{questionnaire}/response/create',    'ResponseController@create')->name('create_response');
    Route::get('/questionnaires/response/delete/{response}',        'ResponseController@destroy')->name('delete_response');
    Route::post('questionnaires/{response}/response/update',        'ResponseController@update')->name('update_response');
    Route::post('/questionnaires/{questionnaire}/response/store',   'ResponseController@store')->name('store_response');


    Route::get('/products',                                'ProductController@index')->name('show_all_products');
    Route::get('/products/create',                         'ProductController@create')->name('create_product');
    Route::get('/products/{product}/edit',                 'ProductController@edit')->name('edit_product');
    Route::get('/products/{product}/delete',               'ProductController@destroy')->name('delete_product');
    Route::post('/products/{product}/update',              'ProductController@update')->name('update_product');
    Route::get('/products/promotion-status{product}',      'ProductController@set_product_promotion_status')->name('set_promotion_status');
    Route::post('/products/store',                         'ProductController@store')->name('store_product');

    
    Route::get('/products/category',                    'ProductCategoryController@index')->name('show_all_product_categories');
    Route::get('/products/category/create',             'ProductCategoryController@create')->name('create_product_category');
    Route::get('/products/category/{category}/edit',    'ProductCategoryController@edit')->name('edit_product_category');
    Route::get('/products/category/{category}/delete',  'ProductCategoryController@destroy')->name('delete_product_category');
    Route::post('/products/category/{category}/update', 'ProductCategoryController@update')->name('update_product_category');
    Route::post('/products/category/store',             'ProductCategoryController@store')->name('store_product_category');
    Route::get('/products/get-main-category/{for}',     'ProductCategoryController@get_main_category')->name('get_product_main_category');


    Route::get('/products/subcategory/create',                  'PSubCategoryController@create')->name('create_product_sub_category');
    Route::get('/products/subcategory/delete/{sub_category}',   'PSubCategoryController@destroy')->name('delete_product_sub_category');
    Route::get('/products/subcategory/edit/{sub_category}',     'PSubCategoryController@edit')->name('edit_product_sub_category');
    Route::post('/products/subcategory/update/{sub_category}',  'PSubCategoryController@update')->name('update_product_sub_category');
    Route::post('/products/{category}/subcategory/store',       'PSubCategoryController@store')->name('store_product_sub_category');

    Route::get('/clinics',                      'ClinicController@index')->name('show_all_clinics');
    Route::get('/clinics/create',               'ClinicController@create')->name('create_clinic');
    Route::get('/clinics/{clinic}/edit',        'ClinicController@edit')->name('edit_clinic');
    Route::get('/clinics/{clinic}/delete',      'ClinicController@destroy')->name('delete_clinic');
    Route::post('/clinics/{clinic}/update',     'ClinicController@update')->name('update_clinic');
    Route::post('/clinics/store',               'ClinicController@store')->name('store_clinic');

    Route::get('/clinics/category',                       'ClinicCategoryController@index')->name('show_all_clinic_categories');
    Route::get('/clinics/category/create',                'ClinicCategoryController@create')->name('create_clinic_category');
    Route::get('/clinics/category/{category}/edit',       'ClinicCategoryController@edit')->name('edit_clinic_category');
    Route::get('/clinics/category/{category}/delete',     'ClinicCategoryController@destroy')->name('delete_clinic_category');
    Route::post('/clinics/category/{category}/update',    'ClinicCategoryController@update')->name('update_clinic_category');
    Route::post('/clinics/category/store',                'ClinicCategoryController@store')->name('store_clinic_category');


    Route::get('/blogs/categories', 'BlogCategoryController@index')->name('show_all_blog_categories');
    Route::get('/blogs/categories/create', 'BlogCategoryController@create')->name('create_blog_category');
    Route::post('/blogs/categories/store', 'BlogCategoryController@store')->name('store_blog_category');
    Route::get('/blogs/categories/delete/{category}', 'BlogCategoryController@destroy')->name('delete_blog_category');
    Route::get('/blogs/get-main-category/{for}',     'BlogCategoryController@get_main_category')->name('get_blog_main_category');

    Route::get('/blogs/subcategory/create',                  'BSubCategoryController@create')->name('create_blog_sub_category');
    Route::get('/blogs/subcategory/delete/{sub_category}',   'BSubCategoryController@destroy')->name('delete_blog_sub_category');
    Route::get('/blogs/subcategory/edit/{sub_category}',     'BSubCategoryController@edit')->name('edit_blog_sub_category');
    Route::post('/blogs/subcategory/update/{sub_category}',  'BSubCategoryController@update')->name('update_blog_sub_category');
    Route::post('/blogs/{category}/subcategory/store',       'BSubCategoryController@store')->name('store_blog_sub_category');

    Route::get('/blogs/', 'BlogController@index')->name('show_all_blogs');
    Route::get('/blogs/create', 'BlogController@create')->name('create_blog');
    Route::get('/blogs/show/blog-{blog}', 'BlogController@show')->name('show_blog');
    Route::get('/blogs/edit/blog-{blog}', 'BlogController@edit')->name('edit_blog');
    Route::get('/blogs/delete/blog-{blog}', 'BlogController@destroy')->name('delete_blog');
    Route::post('/blogs/update/{blog}', 'BlogController@update')->name('update_blog');
    Route::post('/blogs/store', 'BlogController@store')->name('store_blog');

});

Route::namespace('User')->name('user.')->group(function() {
    Route::get('/',                                        'WelcomeController@index')->name('welcome');
    Route::get('/questionnaires',                          'QuestionnaireController@index')->name('show_all_questionnaires');
    Route::get('/questionnaires/{questionnaire}/show',     'QuestionnaireController@show')->name('show_questionnaire');
    
    Route::get('/blogs',                                    'BlogController@index')->name('show_all_blogs');
    Route::get('/blogs/blog-{blog}',                        'BlogController@show')->name('show_blog');

    Route::get('/products',                                 'ProductController@index')->name('show_all_products');
    Route::get('/products/{sub_category}',                  'ProductController@show_sub_category')->name('show_products_under_sub_category');

    Route::get('/clinics',                                  'ClinicController@index')->name('show_all_clinics');

    Route::get('/fitness',                                   'FitnessController@index')->name('show_fitness_page');

    Route::get('/psychology',                                   'PsychologyController@index')->name('show_psychology_page');
});

