@extends('admin.layouts.app')

@section('content')

  <div class="pull-right mt-3">
      <a href="{{ route('show_questionnaire', $questionnaire->id) }}">
          <i class="far fa-arrow-alt-circle-left text-info display-4"></i>
      </a>
  </div>


    <div class="row mt-3">
        <div class="col-md-6 offset-md-3 border shadow py-4 px-5">

        @include('admin.inc.errors.backend_errors')

            <form id="create_qusetion_form" action="{{ route('store_question', $questionnaire->id) }}" method="post">
                @csrf

                <div class="input-group">

                    <div class="input-group-prepend">
                        <div class="input-group-text bg-light p-2">
                            <i class="fas fa-pencil-alt text-info"></i>
                        </div>
                    </div>

                  <label for="questionnaire_content" class=""></label>

                  <textarea 
                        name="questionnaire_contents[question_response]" 
                        id="questionnaire_content" 
                        class="form-control"
                        placeholder="Provide a question for the {{ $questionnaire->name }} questionnaire." 
                        cols="10" 
                        rows="3" 
                        aria-describedby="questionnaire_contents_help">{{ old('questionnaire_contents.question_response') }}</textarea>
                  
                  <input 
                      type="hidden" 
                      name="questionnaire_contents[type]" 
                      value="question">

                </div>
                
                <div class="row align-items-center">

                    <div class="col-md-12 my-4">
                        <a class="btn btn-outline-dark btn-block" id="add_answer">Add Answer</a>
                    </div>

                    <div class="form-group col-md-12">
                        <label class="delete-answer" for="answer_1">
                            <i class="fa fa-times" aria-hidden="true"></i>
                        </label>
                        
                        <input 
                            type="text" 
                            name="choices[0][choice]"
                            value="{{ old('choices.0.choice') }}" 
                            class="form-control answer"
                            placeholder="Choice" 
                            id="answer_1">
                    </div>

                    <div class="form-group col-md-12">
                        <label class="delete-answer" for="answer_2">
                            <i class="fa fa-times" aria-hidden="true"></i>
                        </label>
                        
                        <input 
                            type="text" 
                            name="choices[1][choice]"
                            value="{{ old('choices.1.choice') }}" 
                            class="form-control answer"
                            placeholder="Choice" 
                            id="answer_2">
                    </div>

                </div>
                
                <div id="answers_container"></div>

                <button id="save_questionnaire" type="submit" class="btn btn-outline-info mt-4 btn-block">Save Question</button>
  
              </form>

        </div>
    </div>

@endsection

@section('blade_scripts')
  <script src="{{ asset('admin/js/questions/questions.js') }}" defer></script>    
@endsection