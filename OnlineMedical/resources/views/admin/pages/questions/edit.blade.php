{{-- @extends('admin.layouts.app')

@section('content')

  @include('admin.inc.errors.backend_errors')

    <div class="row">
        <div class="col-md-6 offset-md-3">
            <form id="create_qusetion_form" action="{{ route('update_question', $question->id) }}" method="post">
                
                @csrf
                <div class="form-group">
                  <label for="questionnaire_content" class="">Question:</label>
                  <textarea 
                        name="questionnaire_contents[question_response]" 
                        id="questionnaire_content" 
                        cols="10" 
                        rows="2" 
                        class="form-control" 
                        aria-describedby="questionnaire_contents_help">{!! $question->question_response !!}</textarea>

                  <small id="questionnaire_contents_help" class="form-text text-muted">
                      Provide a question for the <strong>{{ $question->name }}</strong> questionnaire.
                  </small>
                  
                  <input 
                      type="hidden" 
                      name="questionnaire_contents[type]" 
                      value="question">
                </div>
                
                <div class="row align-items-center">

                  <div class="col-md-12">
                    <a class=" mt-3 btn btn-dark text-white btn-block" id="add_answer">Add Answer</a>
                  
                    <small class="form-text text-muted">
                        Click the button to add answers/choices to your question
                    </small>
                  </div>

                    @foreach ($question->choices as $key => $choice)
                      <div class="form-group mt-3 col-md-12">

                        <label class="delete-answer" for="answer{{ $choice->id }}"><i class="fa fa-times" aria-hidden="true"></i> Choice</label>
                        <input 
                            type="text" 
                            name="choices[{{ $key }}][choice]"
                            value="{{ $choice->choice }}" 
                            class="form-control answer" 
                            id="answer{{ $choice->id }}">

                          <input 
                              type="hidden" 
                              name="choices[{{ $key }}][id]" 
                              value="{{ $choice->id }}">
                      </div>
                    @endforeach

                </div>
                
                <div id="answers_container"></div>

                <button id="save_questionnaire" type="submit" class="btn btn-block btn-warning mt-4">Save Question</button>
  
              </form>
        </div>
    </div>
  
@section('blade_scripts')
  <script src="{{ asset('admin/js/questions/questions.js') }}" defer></script>    
@endsection

@endsection --}}




@extends('admin.layouts.app')

@section('content')

  {{-- <div class="pull-right mt-3">
      <a href="{{ route('show_questionnaire', $questionnaire->id) }}">
          <i class="far fa-arrow-alt-circle-left text-info display-4"></i>
      </a>
  </div> --}}


    <div class="row mt-3">
        <div class="col-md-6 offset-md-3 border shadow py-4 px-5">

        @include('admin.inc.errors.backend_errors')

            <form id="create_qusetion_form" action="{{ route('update_question', $question->id) }}" method="post">
                @csrf

                <div class="input-group">

                    <div class="input-group-prepend">
                        <div class="input-group-text bg-light p-2">
                            <i class="fas fa-pencil-alt text-info"></i>
                        </div>
                    </div>

                  <label for="questionnaire_content" class=""></label>

                  <textarea 
                        name="questionnaire_contents[question_response]" 
                        id="questionnaire_content" 
                        class="form-control"
                        cols="10" 
                        rows="3" 
                        aria-describedby="questionnaire_contents_help">{!! $question->question_response !!}</textarea>
                  
                  <input 
                      type="hidden" 
                      name="questionnaire_contents[type]" 
                      value="question">

                </div>
                
                <div class="row align-items-center">

                    <div class="col-md-12 my-4">
                        <a class="btn btn-outline-dark btn-block" id="add_answer">Add Answer</a>
                    </div>

                    @foreach ($question->choices as $key => $choice)
                        <div class="form-group mt-3 col-md-12">

                          <label class="delete-answer" for="answer{{ $choice->id }}">
                              <i class="fa fa-times" aria-hidden="true"></i>
                          </label>

                          <input 
                              type="text" 
                              name="choices[{{ $key }}][choice]"
                              value="{{ $choice->choice }}" 
                              class="form-control answer" 
                              id="answer{{ $choice->id }}">

                            <input 
                                type="hidden" 
                                name="choices[{{ $key }}][id]" 
                                value="{{ $choice->id }}">
                        </div>
                    @endforeach

                </div>
                
                <div id="answers_container"></div>

                <button id="save_questionnaire" type="submit" class="btn btn-outline-info mt-4 btn-block">Save Question</button>
  
              </form>

        </div>
    </div>

@endsection

@section('blade_scripts')
  <script src="{{ asset('admin/js/questions/questions.js') }}" defer></script>    
@endsection