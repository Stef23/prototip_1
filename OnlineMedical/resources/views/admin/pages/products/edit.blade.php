@extends('admin.layouts.app')


@section('content')

<div class="pull-right mt-3">
    <a href="{{ url()->previous() }}">
        <i class="far fa-arrow-alt-circle-left text-info display-4"></i>
    </a>
</div>

<div class="row text-center my-5">
    <div class="col-md-6 offset-md-3">

        @include('admin.inc.errors.backend_errors')

        <div class="card shadow">
            <div class="card-header text-info text-center text-uppercase shadow-sm">
                <h4 class="m-0">Edit Product</h4>
            </div>

            <div class="card-body">
                <form id="create_product_form" action="{{ route('update_product', $product->id) }}" method="post" enctype="multipart/form-data">
                    @csrf

                    <div class="input-group mb-3">
                        <label class="btn btn-light border btn-block text-muted" for="product_image">
                            Change the image of your product
                        </label>

                        <input 
                        type="file" 
                        name="image"
                        value="{{ $product->image }}" 
                        class="custom-file-input d-none" 
                        id="product_image">
                    </div>


                    <div class="input-group mb-4">
                        <div class="input-group-prepend">
                            <div class="input-group-text bg-light">
                                <i class="fab fa-tumblr text-info"></i>
                            </div>
                        </div>
                        
                        <label for="product_name"></label>
                        
                        <input 
                            type="text" 
                            name="name" 
                            value="{{ $product->name }}" 
                            id="product_name" 
                            class="form-control" 
                            autocomplete="off" 
                            placeholder="Provide a name for your product" 
                            aria-describedby="name_help">
                    </div>

                    <div class="input-group mb-2">
                        <div class="input-group-prepend">
                            <div class="input-group-text bg-light p-2">
                                <i class="fas fa-pencil-alt text-info"></i>
                            </div>
                        </div>

                        <label for="product_short_description"></label>
                        
                        <textarea 
                            name="short_description" 
                            id="product_short_description" 
                            cols="30" 
                            rows="1" 
                            class="form-control" 
                            placeholder="Provide a short description for your product" 
                            aria-describedby="short_description_help">{{ $product->short_description }}</textarea>
                    </div>

                    <div class="form-group mb-4">
                        <label for="product_description"></label>
                        <textarea 
                            name="description" 
                            id="product_description" 
                            class="form-control" 
                            cols="30" 
                            rows="5" 
                            placeholder="Provide a description for your product" 
                            aria-describedby="description_help">{{ $product->description }}</textarea>
                    </div>

                    <div class="input-group mb-4">
                        <div class="input-group-prepend">
                            <label class="input-group-text bg-light" for="category">
                                <i class="fas fa-sort text-info"></i>
                            </label>
                        </div>
                        
                        <label for="category"></label>
                        
                        <select name="p_sub_category_id" id="category" class="form-control">
                            @forelse ($sub_categories as $sub_category)
                                <option 
                                    value="{{$sub_category->id}}" 
                                    {{ $sub_category->id == $product->p_sub_category_id ? 'selected' : '' }}>
                                    {{$sub_category->sub_category}}
                                </option>
                            @empty
                                <option disabled value="">No categories added</option>                                
                            @endforelse
                        </select>
                    </div>

                    <button class="btn btn-block btn-outline-info">Store</button>

                </form>
            </div>
        </div>
    </div>
</div>


@endsection

@section('blade_scripts')
    <script src="{{ asset('admin/js/products/products.js') }}" defer></script>
@endsection