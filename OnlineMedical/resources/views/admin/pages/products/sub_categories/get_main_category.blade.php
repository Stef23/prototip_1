@extends('admin.layouts.app')


@section('content')

@include('admin.inc.errors.backend_errors')

<div class="row animated fadeIn min-vh-100 justify-content-center align-items-center">
    <div class="col-md-6 shadow">

        <div class="jumbotron m-4">
            <h6 class="text-center text-info"> 
                <b>Please choose from below, under which category you would like to add your sub-categories</b>
            </h6>
        </div>
        
        <form action="{{ $route }}" method="get">
            <div class="form-group">

                <select 
                    name="category" 
                    class="form-control" 
                    id="">
                    
                    @forelse ($categories as $category)
                        <option value="{{$category->id}}">{{$category->category}}</option>
                    @empty
                        <option value="">You must have at least one main category so you can continue.</option>
                    @endforelse
                </select>

                <button class="btn pull-right">Continue</button>
            </div>
        </form>

    </div>
</div>
@endsection