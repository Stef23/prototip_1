@extends('admin.layouts.app')


@section('content')

    <div class="pull-right mt-3">
        <a href="{{ url()->previous() }}">
            <i class="far fa-arrow-alt-circle-left text-info display-4"></i>
        </a>
    </div>
    

    <div class="row animated fadeIn min-vh-100 justify-content-center align-items-center ">
        <div class="col-md-6 text-center">

            @include('admin.inc.errors.backend_errors')
            @include('admin.inc.successes.success')
            
            <div class="card shadow">

                <div class="card-header text-info text-center text-uppercase shadow-sm">
                    <small>Main category:</small>
                    <b>{{ $sub_category->p_category->category }}</b>
                </div>

                <div class="card-body">

                    <form id="create_product_sub_category_form" action="{{ route('update_product_sub_category', $sub_category->id) }}" method="post">
                        @csrf

                        <div class="input-group py-3 mb-4">
                            <div class="input-group-prepend">
                                <div class="input-group-text bg-light">
                                    <i class="fab fa-cuttlefish text-info"></i>
                                </div>
                            </div>

                            <input 
                                type="text" 
                                name="sub_category" 
                                placeholder="Please enter a sub-category"
                                class="form-control"
                                value="{{ $sub_category->sub_category }}">
                                
                            </div>

                        <button class="btn btn-block btn-outline-info ">Save</button>
                    </form>

                </div>

            </div>
        </div>
    </div>
@endsection

@section('blade_scripts')
    <script src="{{ asset('admin/js/products/category/sub_category.js') }}" defer></script>
@endsection