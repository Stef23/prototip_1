@extends('admin.layouts.app')

@section('content')
<div id="our_products" class="row">

    <div class="col-md-12 text-center p-0">
        <h1 class="my-2 text-capitalize">Products</h1>


        <nav class="navbar navbar-expand-lg navbar-light bg-light">

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse justify-content-center" id="navbarNavDropdown">
              <ul class="nav navbar-nav">

                <li class="nav-item active">
                    <a class="nav-link active text-info" href="{{ route('show_all_products') }}">All <span class="sr-only">(current)</span></a>
                </li>
               
                @forelse ($categories as $category)                

                    <li class="nav-item dropdown">
                        <a 
                        class="nav-link dropdown-toggle text-info pr-3"
                        href="#" id="navbarDropdownMenuLink" 
                        data-toggle="dropdown" 
                        aria-haspopup="true" 
                        aria-expanded="false">
                        {{ $category->category }}
                        </a>

                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            @forelse ($category->sub_categories as $sub_category)
                                <a class="dropdown-item text-info p-2" href="?sub_category={{$sub_category->id}}">{{$sub_category->sub_category}}</a>
                            @empty

                            <ul class="nav navbar-nav">
                                <li class="nav-item dropdown">
                                    <small  class="nav-link text-info">No subcategory added</small>
                                </li>
                            </ul>
                            
                            @endforelse
                        </div>

                    </li>
                @empty
                    
                @endforelse

              </ul>
            </div>
        </nav>
    </div>

    <div class="col-md-10 offset-md-1">
        
        @include('admin.inc.successes.success')

        <div class="row"> 

            @forelse ($products as $product)
                <div class="col-md-4 my-3">
                    <div class="card p-0 shadow">

                        <div class="card-body p-0 cursor-pointer" data-toggle="modal" data-target="#myDescriptionModal{{ $product->id }}">
                            <img class="img-fluid" src="{{ asset('storage/'.$product->image) }}" alt="">
                            <div class="p-3">
                                <h5 class="my-2">{{ $product->name }}</h5>
                                <p>{{ $product->short_description }}</p>
                                <br>
                                <small> Sub category: {{ $product->p_sub_category->sub_category }}</small>
                                <br>
                                <small>Status:</small>
                                @if ($product->promoted)
                                    <small class="text-success"> Promoted</small>
                                @else
                                    <small class="text-muted" title="Demote this product"> Neutral</small>
                                @endif
                            </div>
                        </div>

                        <div class="card-action p-3 d-flex justify-content-between">
                            <a href="{{ route('edit_product', $product->id) }}" class="">
                                <i class="far fa-edit text-warning"></i>
                            </a>
                            <a href="{{ route('set_promotion_status', $product->id) }}" class="">
                                <i class="text-center fas {{ $product->promoted ? 'fa-times-circle text-muted' : 'fa-check-circle text-info'}}">
                                    <br>
                                    <small> {{ $product->promoted ? 'Demote' : 'Promote' }} </small>
                                </i>
                            </a>
                            <a href="{{ route('delete_product', $product->id) }}" class="">
                                <i class="fas fa-trash-alt text-danger"></i>
                            </a>
                        </div>

                    </div>   
                </div>

                <div id="myDescriptionModal{{ $product->id }}" class="modal fade" role="dialog">
                    <div class="modal-dialog modal-md modal-dialog-centered modal-dialog-scrollable">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">Description</h4>
                            </div>
                            <div class="modal-body">
                                {!! $product->description !!}
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default btn-outline-info" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
                
            @empty

                <div class="text-center text-info w-100 mt-5">
                    <h3>
                        There is no products added yet.
                        <hr class="w-50">
                        <a href="{{ route('get_product_main_category', 'product') }}">
                            <i class="fas fa-notes-medical text-info"> Add One Here</i>
                        </a>
                    </h3>
                </div>  

            @endforelse

        </div>
    </div>
</div>
@endsection