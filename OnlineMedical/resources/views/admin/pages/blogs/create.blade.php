@extends('admin.layouts.app')


@section('content')

<div class="row my-5">
    <div class="col-md-8 offset-md-2">

        @include('admin.inc.errors.backend_errors')

        <div class="card shadow text-center">
            <div class="card-header text-info text-center text-uppercase shadow-sm">
                <h4 class="m-0">Create a new blog</h4>
            </div>

            <div class="card-body">
                <form id="create_blog_form" action="{{ route('store_blog') }}" method="post" enctype="multipart/form-data">
                    @csrf

                    <div class="input-group">
                        <label class="btn btn-light border btn-block text-muted mb-4" for="blog_cover_image">
                            Choose an image as a cover for your blog
                        </label>

                        <input 
                        type="file" 
                        name="cover_image" 
                        class="custom-file-input d-none" 
                        id="blog_cover_image">
                    </div>


                    <div class="input-group mb-4">
                        <div class="input-group-prepend">
                            <div class="input-group-text bg-light">
                                <i class="fab fa-tumblr text-info"></i>
                            </div>
                        </div>

                        <label for="blog_title"></label>

                        <input 
                            type="text" 
                            name="title" 
                            value="{{ old('title') }}" 
                            id="blog_title" 
                            class="form-control" 
                            autocomplete="off" 
                            placeholder="Provide a title for your blog">
                    </div>


                    <div class="form-group mb-5">
                        <label for="blog_body"></label>

                        <textarea 
                            name="body" 
                            id="blog_body" 
                            class="form-control" 
                            cols="30" 
                            rows="10" 
                            placeholder="Provide a body for your blog">{{ old('body') }}</textarea>
                    </div>


                    <div class="input-group mb-4">
                        <div class="input-group-prepend">
                            <div class="input-group-text bg-light p-0">
                                <i class="fas fa-tags text-info p-2"></i>
                            </div>
                        </div>

                        <label for="blog_title"></label>
                        <input 
                            type="text" 
                            name="blog_tags" 
                            value="{{ old('blog_tags') }}" 
                            id="blog_tags" 
                            class="form-control" 
                            autocomplete="off" 
                            placeholder="Provide your tags separated with comma" 
                            aria-describedby="blog_tags_help">
                    </div>


                    <div class="input-group mb-4">
                        <div class="input-group-prepend">
                            <label class="input-group-text bg-light" for="sub_category">
                                <i class="fas fa-sort text-info"></i>
                            </label>
                        </div>

                        <select 
                            name="b_sub_category_id" 
                            id="sub_category" 
                            class="form-control">

                            @forelse ($sub_categories as $sub_category)                                
                                <option value="{{ $sub_category->id }}">{{ $sub_category->sub_category }}</option>
                            @empty
                                <option disabled value="">No categories added</option>                                
                            @endforelse
                        </select>
                    </div>


                    <button class="btn btn-block  btn-outline-info">Store</button>

                </form>
            </div>
        </div>
    </div>
</div>


@endsection


@section('blade_scripts')
    <script src="{{ asset('admin/js/blogs/blogs.js') }}" defer></script>    
@endsection