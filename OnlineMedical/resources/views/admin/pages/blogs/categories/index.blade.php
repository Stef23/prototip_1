@extends('admin.layouts.app')

@section('content')


    <div class="row mt-3">
        <div class="col-md-6 offset-md-3">

            @include('admin.inc.errors.backend_errors')
            @include('admin.inc.successes.success')

            <div class="jumbotron text-center text-info text-uppercase shadow-sm mb-3">
                <h3 class="m-0">blog categories</h3>
            </div>

            <ul class="list-group">
                
                @forelse ($categories as $key => $category)

                <li class="list-group-item mb-2 border shadow-sm">

                    <div class="blogs-action-container">
                        
                        <div class="d-flex justify-content-between">
                            <a 
                               type="button" 
                               class="text-info"
                               data-toggle="collapse" 
                               data-target="#blogsActionCollapse{{ $key }}" 
                               aria-expanded="false" 
                               aria-controls="blogsActionCollapse">
                               {{ $category->category }} 
                               <i class="fas fa-eye"></i>
                            </a>

                            <small>  
                                This category has 
                                <strong class="text-info">
                                    ( {{ $category->sub_categories_count }} )
                                </strong> sub_categories under it.
                            </small>
                        

                            <div class="d-flex align-items-center">
                                {{-- <a class="" title="Edit Category" href="{{ route('edit_blog_category', $category->id) }}">
                                    <i class="far fa-edit text-warning"></i>
                                </a> --}}
                                <a class="ml-2" title="Delete Category" href="{{ route('delete_blog_category', $category->id) }}">
                                    <i class="fa fa-trash-alt text-danger"></i>
                                </a>
                            </div>
                        </div>
        
                            <div class="collapse" id="blogsActionCollapse{{ $key }}">
                                    
                            <ul class="list-group">

                                @forelse ($category->sub_categories as $sub_category)
                                    <li class="list-group-item d-flex justify-content-between p-1 mb-1 border shadow-sm">
                                                                                
                                        <div class="d-flex align-items-center">
                                            <a class="" title="Edit Category" href="{{ route('edit_blog_sub_category', $sub_category->id) }}">
                                                <i class="far fa-edit text-muted"></i>
                                            </a>
                                            <a class="ml-2" title="Delete Category" href="{{ route('delete_blog_sub_category', $sub_category->id) }}">
                                                <i class="fa fa-trash-alt text-muted"></i>
                                            </a>
                                        </div>

                                        <span class="dropdown-item text-info text-center">{{$sub_category->sub_category}}</span>

                                    </li>
                                @empty
                                
                                @endforelse

                            </ul>
                        </div>
                        
                    </div>
                </li>

                @empty
                
                    <div class="text-center text-info w-100 mt-5">
                        <h3>
                            There is no categories added yet.
                            <hr class="w-50">
                            <a href="{{ route('create_blog_category') }}">
                                <i class="fas fa-notes-medical text-info"> Add One Here</i>
                            </a>
                        </h3>
                    </div> 
                    
                @endforelse

            </ul>
        </div>
    </div>
    
@endsection