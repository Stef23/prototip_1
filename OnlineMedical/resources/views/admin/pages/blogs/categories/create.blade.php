@extends('admin.layouts.app')

@section('content')

    <div class="row min-vh-100 justify-content-center align-items-center">
        <div class="col-md-6 text-center">

            @include('admin.inc.errors.backend_errors')
            @include('admin.inc.successes.success')

            <div class="card shadow">

                <div class="card-header text-info text-center text-uppercase shadow-sm">
                    <h5 class="m-0">Create a category for your blogs</h5>
                </div>

                <div class="card-body">
                
                    <form id="create_blog_category_form" action="{{ route('store_blog_category') }}" method="post">
                        @csrf

                        <div class="input-group py-3 mb-4">
                            <div class="input-group-prepend">
                                <div class="input-group-text bg-light">
                                    <i class="fab fa-cuttlefish text-info"></i>
                                </div>
                            </div>
                            <select name="section_id" class="form-control">
                                @foreach ($sections as $section)
                                    <option value="{{$section->id}}">{{$section->section}}</option>                                    
                                @endforeach

                            </select>
                                
                        </div>

                        <div class="input-group py-3 mb-4">
                            <div class="input-group-prepend">
                                <div class="input-group-text bg-light">
                                    <i class="fab fa-cuttlefish text-info"></i>
                                </div>
                            </div>

                            <input 
                                type="text" 
                                name="category" 
                                placeholder="Provide a category that you can store your blogs under" 
                                class="form-control" 
                                aria-describedby="product_category_help">

                            <div class="error-container"></div>
                        </div>

                        <button type="submit" class="btn btn-outline-info btn-block">Save</button>
                    </form>

                </div>
            </div>
        </div>
    </div>
    
@endsection


@section('blade_scripts')
    <script src="{{ asset('admin/js/blogs/category/category.js') }}" defer></script>
@endsection