@extends('admin.layouts.app')

@section('content')

    <div class="row min-vh-100 justify-content-center align-items-center">
        <div class="col-md-6 text-center">

            @include('admin.inc.errors.backend_errors')
            @include('admin.inc.successes.success')

            <div class="card shadow">

                <div class="card-header text-info text-center text-uppercase shadow-sm">
                    <h5 class="m-0">Edit the category for your blogs</h5>
                </div>

                <div class="card-body">
                
                    <form id="create_product_category_form" action="{{ route('update_blog_sub_category', $sub_category->id) }}" method="post">
                        @csrf

                        <div class="input-group py-3 mb-4">
                            <div class="input-group-prepend">
                                <div class="input-group-text bg-light">
                                    <i class="fab fa-cuttlefish text-info"></i>
                                </div>
                            </div>

                            <input 
                                type="text" 
                                name="sub_category"
                                value="{{ $sub_category->sub_category }}" 
                                placeholder="Provide a sub_category that you can store your blogs under" 
                                class="form-control" 
                                aria-describedby="blog_sub_category_help">

                            <div class="error-container"></div>
                        </div>

                        <button type="submit" class="btn btn-outline-info btn-block">Save</button>
                    </form>

                </div>
            </div>
        </div>
    </div>
    
@endsection


@section('blade_scripts')
    <script src="{{ asset('admin/js/products/category/category.js') }}" defer></script>
@endsection