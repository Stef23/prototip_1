@extends('admin.layouts.app')


@section('content')


<div class="row text-center my-5">
    
    <div class="col-md-8 offset-md-2">
        @include('admin.inc.errors.backend_errors')
        @include('admin.inc.successes.success')
    </div>

        @forelse ($blogs as $blog)
            <div class="col-md-4 my-3">
                <a href="{{route('show_blog', $blog->id)}}" class="text-decoration-none text-dark">
                    
                    <div class="card border-0 shadow">
                        <div class="card-body min-vh-25 p-0 cover-bg" 
                             style="background-image: url({{asset('storage/'.$blog->cover_image)}});">
                        </div>

                        <div class="card-footer bg-white py-4">
                            <p class="h3"><strong>{{$blog->title}}</strong></p>
                            <hr>
                            <a href="{{ route('edit_blog', $blog->id) }}" class="pull-left">
                                <i class="far fa-edit text-warning"></i>
                            </a>
                            <a href="{{ route('delete_blog', $blog->id) }}" class="pull-right">
                                <i class="fas fa-trash text-danger"></i>
                            </a>
                        </div>
                    </div>

                </a>
            </div>   
                                         
        @empty

            <div class="text-center text-info w-100 mt-5">
                <h3>
                    There are no blogs added yet.
                    <hr class="w-50">
                    <a href="{{ route('get_blog_main_category', 'blog') }}">
                        <i class="fab fa-blogger text-info"> Add One Here</i>
                    </a>
                </h3>
            </div>
            
        @endforelse
    
</div>
@endsection