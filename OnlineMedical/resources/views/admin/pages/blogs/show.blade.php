@extends('admin.layouts.app')

@section('blade_css')
    <link href="{{ asset('admin/css/blogs/show.css') }}" rel="stylesheet">    
@endsection

@section('content')

    <div class="">
        <div class="row min-vh-100 cover-bg" style="background-image: url({{asset('storage/'.$blog->cover_image)}});"></div>
        
        <div id="blog-main-content-container" class="row">
            <div id="blog-title" class="border-bottom border-success col-lg-6 offset-lg-3 col-md-8 offset-md-2 min-vh-50 shadow-lg d-flex align-items-center justify-content-center">
                <h1>{{$blog->title}}</h1>
            </div>
            <div id="blog-body" class="col-lg-6 offset-lg-3 col-md-8 offset-md-2">
                {!! $blog->body !!}
            </div>
        </div>

        <div class="custom-hr"></div>

        <div class="row">
            <div class="col-md-8 offset-md-2">
                <p class="h1 text-center">Споделете:</p>
                <p class="text-center h2 text-success">
                    <i class="fab fa-facebook-square"></i>
                    <i class="fab fa-linkedin"></i>
                </p>
            </div>
        </div>
        
    </div>
    
@endsection