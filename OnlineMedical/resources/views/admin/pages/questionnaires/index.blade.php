@extends('admin.layouts.app')


@section('content')
    <div class="row">
        <div class="col-md-8 offset-md-2">
            
            <div class="jumbotron my-4 p-1 text-light bg-info text-uppercase">
                <h1 class="text-center m-0"><b>All questionnaires</b></h1>
            </div>
            
            @forelse ($questionnaires as $questionnaire)
                <div class="jumbotron shadow-sm border bg-light mb-3">
                    <div class="row flex-row align-items-center justify-content-between">
                        
                        <div class="col-md-8 p-3">
                            <h3 class="m-0">{{ $questionnaire->name }}</h3>
                            <p>
                                <small>
                                    {!! $questionnaire->description !!}
                                </small>
                            </p>

                            <a href="{{route('show_questionnaire', $questionnaire->id)}}" 
                               title="Show Questionnaire" 
                               class="text-info btn border">
                                Show
                            </a>        
                            
                            @if (!$questionnaire->in_production)
                                <a href="{{ route('edit_questionnaire', $questionnaire->id) }}" 
                                   title="Edit Questionnaire" 
                                   class="text-info btn border">
                                    Edit
                                </a>

                                <a title="Delete Questionnaire" 
                                class="text-info btn border" 
                                type="button" 
                                href="#" 
                                data-toggle="modal" 
                                data-target="#deleteModal">
                                Delete
                                </a>  
                            @endif
                            
                        </div>
                        
                        <div class="col-md-4 text-center border shadow-sm p-3">
                            <p class="display-4 text-info m-0">{!! $questionnaire->category->icon->icon !!}</p>
                            <p class="text-info">{!! $questionnaire->category->category !!}</p>
                            
                            <div class="{{$questionnaire->in_production ? "alert alert-info m-0 p-1" : 'd-none'}}" role="alert">
                                <h5 class="alert-heading text-center m-0">
                                    {{$questionnaire->in_production ? "Published!" : ''}}
                                </h5>
                            </div>
                        </div>

                    </div>
                </div>

                <!-- Modal -->
                @include('admin.inc.modals.delete_modal', [
                    'route' => route('delete_questionnaire', $questionnaire->id),
                    'deleteModalId' => 'deleteModal',
                    'deleteModalLabel' => 'deleteModalLabel',
                    'deleteSubject' => $questionnaire->name
                    ])

            @empty

                <div class="text-center text-info w-100 mt-5">
                    <h3>
                        There are no questionnaires added yet.
                        <hr class="w-50">
                        <a href="{{ route('create_questionnaire') }}">
                            <i class="fas fa-notes-medical text-info"> Add One Here</i>
                        </a>
                    </h3>
                </div> 

            @endforelse    
        </div>        
    </div>
@endsection