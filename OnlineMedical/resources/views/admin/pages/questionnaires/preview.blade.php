@extends('admin.layouts.app')


@section('content')

        <div data="{{$questionnaire->id}}" class="row h-100 flex-column justify-content-center animated fadeIn" id="questionnaire_content">
        
        </div>


@section('blade_scripts')

        <script src="{{ asset('admin/js/questionnaire/questionnaire_preview.js') }}" defer></script>

@endsection

@endsection