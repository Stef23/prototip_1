@extends('admin.layouts.app')


@section('content')

<div class="row my-5">
    <div class="col-md-6 offset-md-3">

        @include('admin.inc.errors.backend_errors')

        <div class="card shadow text-center">

            <div class="card-header text-info text-center text-uppercase shadow-sm">
                <h5 class="m-0">Edit Questionnaire</h5>
            </div>

            <div class="card-body">
                <form id="create_questionnaire_form" action="{{ route('update_questionnaire', $questionnaire->id) }}" method="post">
                    @csrf

                    <div class="input-group mb-4">
                        <div class="input-group-prepend">
                            <div class="input-group-text bg-light">
                                <i class="fab fa-tumblr text-info"></i>
                            </div>
                        </div>

                        <label for="blog_title"></label>
                        
                        <input
                            type="text" 
                            name="name" 
                            id="questionnaire_title" 
                            class="form-control" 
                            value="{{ $questionnaire->name }}" 
                            placeholder="Provide a descriptive title for your questionnaire">
                    </div>

                    <div class="input-group mb-4">
                        <div class="input-group-prepend">
                            <div class="input-group-text bg-light p-2">
                                <i class="fas fa-pencil-alt text-info"></i>
                            </div>
                        </div>
                        
                        <label for="questionnaire_description"></label>

                        <textarea 
                            name="description" 
                            id="questionnaire_description" 
                            cols="30" 
                            rows="2"
                            class="form-control" 
                            placeholder="Provide a description for your questionnaire">{{ $questionnaire->description }}</textarea>
                    </div>

                   

                    <div class="input-group mb-4">
                        <div class="input-group-prepend">
                            <label class="input-group-text bg-light" for="category">
                                <i class="fas fa-sort text-info"></i>
                            </label>
                        </div>

                        <label for="category"></label>

                        <select 
                            name="q_category_id" 
                            id="category" 
                            class="form-control">
                            
                            @forelse ($categories as $category)                                
                                <option 
                                    value="{{ $category->id }}" 
                                    {{ $category->id == $questionnaire->q_category_id ? 'selected' : '' }}>
                                    {{ $category->category }}
                                </option>
                            @empty
                                <option disabled value="">No categories added</option>                                
                            @endforelse
                        </select>
                    </div>
                        
                    <button class="btn btn-block border-info btn-light text-info">Create</button>
                </form>
            </div>
        </div>
    </div>
</div>

@section('blade_scripts')
    <script src="{{ asset('admin/js/questionnaire/questionnaire.js') }}" defer></script>
@endsection

@endsection