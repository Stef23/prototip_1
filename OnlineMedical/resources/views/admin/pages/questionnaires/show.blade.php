{{-- @extends('admin.layouts.app')


@section('content')

    @include('admin.inc.errors.backend_errors')
    @include('admin.inc.successes.success')

    <div class="row mt-4">
        <div class="col-md-10 offset-md-1">
            <div class="jumbotron border border-info text-dark">
                
                <div class="d-flex justify-content-between">

                    <div>
                        <h1 class="display-4">{{$questionnaire->name}}</h1>
                        <p class="lead">{!! $questionnaire->description !!}</p>
                    </div>

                    <div class="questionnaire-action-container">
                        <a type="button" data-toggle="collapse" data-target="#questionnaireActionCollapse" aria-expanded="false" aria-controls="questionnaireActionCollapse">
                            Questionnaire options <i class="fas fa-cog"></i>
                        </a>
        
                        <div class="collapse text-right" id="questionnaireActionCollapse">

                            @if (!$questionnaire->in_production)
                                <a title="Edit" 
                                class="text-info m-2" 
                                type="button" 
                                href="{{ route('edit_questionnaire', $questionnaire->id) }}"><i class="fas fa-edit"></i>
                                </a>

                                <a title="Delete" 
                                class="text-info m-2" 
                                type="button" 
                                href="#" 
                                data-toggle="modal" 
                                data-target="#deleteModal"><i class="fas fa-trash"></i>
                                </a>  
                            @endif
                            
                            <a title="Preview" 
                                class="text-info m-2" 
                                type="button" 
                                href="{{route('preview_questionnaire', $questionnaire->id)}}"><i class="far fa-share-square"></i>
                            </a>  
                            
                            <a title="{{$questionnaire->in_production ? 'Remove from production' : 'Publish'}}" 
                                class="text-info m-2" 
                                type="button" 
                                href="#"
                                data-toggle="modal"
                                data-target="#setInProductionModal">
                                <i class="fas {{$questionnaire->in_production ? 'fa-lock' : 'fa-lock-open'}}"></i>
                            </a>  
                            
                        </div>

                        <div class="{{$questionnaire->in_production ? "alert alert-info mt-3" : 'd-none'}}" role="alert">
                            <h5 class="alert-heading text-center">
                                {{$questionnaire->in_production ? "Published!" : ''}}
                            </h5>
                        </div>

                    </div>
                </div>

                <hr class="my-4">
                
                <ul class="nav justify-content-around">
                    <li class="nav-item">
                        <a title="Show all questions" class="btn btn-outline-info btn-sm" href="#" class="nav-link" data-toggle="collapse" data-target="#collapseQuestions" aria-expanded="true" aria-controls="collapseQuestions">
                            <i class="far fa-eye"> Questions</i>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a title="Show all responses" class="btn btn-outline-info btn-sm" href="#" class="nav-link" data-toggle="collapse" data-target="#collapseResponses" aria-expanded="true" aria-controls="collapseResponses">
                            <i class="far fa-eye"> Responses</i>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a title="Add new question" class="btn btn-outline-info btn-sm" href="{{route('create_question', $questionnaire->id)}}" class="nav-link">
                            <i class="fas fa-plus-circle"> Question</i>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a title="Add new response" class="btn btn-outline-info btn-sm" href="{{route('create_response', $questionnaire->id)}}" class="nav-link">
                            <i class="fas fa-plus-circle"> Response</i>
                        </a>
                    </li>
                </ul>
                
            </div>
        </div>

        <!-- Modal -->
        @include('admin.inc.modals.delete_modal', [
            'route' => route('delete_questionnaire', $questionnaire->id),
            'deleteModalId' => 'deleteModal',
            'deleteModalLabel' => 'deleteModalLabel',
            'deleteSubject' => $questionnaire->name
            ])

        @include('admin.inc.modals.production_warning_modal', [
            'route' => route('set_questionnaire_in_production', $questionnaire->id),
            'setInProductionModalId' => 'setInProductionModal',
            'setInProductionModalLabel' => 'setInProductionModalLabel',
            'setInProductionSubject' => $questionnaire->name
            ])
    </div>



    <div class="accordion" id="accordionContent">
        <div class="card border-0">
      
          <div id="collapseQuestions" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionContent">
            <div class="card-body">

                <form action="{{ route('make_questionnaire_schema', $questionnaire->id) }}" method="post">        
                    @csrf
                    <h1 class="text-center text-info">Questions</h1>
                    

                        @forelse ($questions as $question)
                            
                            <div class="card border border-info text-dark my-5 p-3 shadow">
                                <div class="card-body m-0">

                                    @if (!$questionnaire->in_production)

                                        <div class="question-action-container pull-right">
                                            <a type="button" data-toggle="collapse" data-target="#questionActionColapse{{$question->id}}" aria-expanded="false" aria-controls="questionActionColapse{{$question->id}}">
                                                Question options <i class="fas fa-cog"></i>
                                            </a>

                                            <div class="collapse text-right" id="questionActionColapse{{$question->id}}">
                                                <a class="text-warning" href="{{route('edit_question', $question->id)}}">
                                                    <i title="Edit Question" class="fas fa-edit"></i>
                                                </a>
                                                <a class="text-danger" href="#" data-toggle="modal" data-target="#deleteModal{{$question->id}}">
                                                    <i title="Delete Question" class="fas fa-trash"></i>
                                                </a>
                                            </div>
                                        </div>

                                    @endif

                                    <label for="{{ $question->id }}">Starter Question</label>

                                    <input 
                                        id="{{ $question->id }}"
                                        type="radio" 
                                        name="starter_question_id[]"
                                        value="{{ $question->id }}"
                                        {{ $question->starter_question == 1 ? 'checked' : '' }}>

                                        <small id="starter_question_help" class="form-text text-muted">Check the radio if you whish <strong>{{ $questionnaire->name }}</strong> questionnaire to start with this question.</small>
                                

                                    <div class="text-center"> {!! $question->question_response !!} </div>

                                    <ul class="list-group list-group-horizontal">
                                        @foreach ($question->choices as $choice)
                                            <li class="list-group-item border-info list-group-item-action">{{$choice->choice}}
                                                <br>
                                                <small>Jump to:</small>
                                                <div class="form-group">
                                                    <select name="question_choices[{{$choice->id}}][jump_to]" class="form-control">

                                                        @forelse ($questionnaire->questions_responses as $question_response)
                                                            <option
                                                                value="{{ $question_response->id }}"
                                                                {{ old('question_choices.'. $choice->id .'.jump_to') == $question_response->id ? 'selected' : '' }}
                                                                {{ $question_response->id == $choice->jump_to ? 'selected' : '' }}>
                                                                {{Str::limit(strip_tags($question_response->question_response),60)}}
                                                                {{$question_response->type}}
                                                            </option>
                                                        @empty
                                                            <option value="null" disabled>No questions or responses added</option>
                                                        @endforelse

                                                    </select>
                                                </div>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
        
                            <!-- Modal -->
                            @include('admin.inc.modals.delete_modal', [
                                'route' => route('delete_question', $question->id),
                                'deleteModalId' => 'deleteModal'.$question->id,
                                'deleteModalLabel' => 'deleteModalLabel'.$question->id,
                                'deleteSubject' => $question->question_response
                                ])

                        @empty
                        
                            <div class="text-center text-info w-100 mt-5">
                                <h3>
                                    There are no questions added yet.
                                    <hr class="w-50">
                                    <a href="{{ route('create_question', $questionnaire->id) }}">
                                        <i class="fas fa-notes-medical text-info"> Add One Here</i>
                                    </a>
                                </h3>
                            </div> 
    
                        @endforelse
                        
                        @if ( count($questions) && (!$questionnaire->in_production) )
                            <button class="btn btn-info" type="submit">Save Questionnaire Schema</button>
                        @endif                            
                </form>

            </div>
          </div>
        </div>

        <div class="card border-0">

          <div id="collapseResponses" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionContent">
            
            <div class="card-body">
                <h1 class="text-center text-info">Responses</h1>

                @forelse ($responses as $response)
                    
                    <div class="card border border-info text-dark mb-3">

                        <div class="card-body">
                            {!! $response->question_response !!}
                        </div>

                        <div class="card-footer text-right">
                            <a href="{{ route('edit_response', $response->id) }}" class="">
                                <i class="far fa-edit text-warning"></i>
                            </a>
                            <a href="{{ route('delete_response', $response->id) }}" class="ml-2">
                                <i class="fa fa-trash text-danger"></i>
                            </a>
                        </div>

                    </div>

                @empty
                
                    <div class="text-center text-info w-100 mt-5">
                        <h3>
                            There are no responses added yet.
                            <hr class="w-50">
                            <a href="{{ route('create_response', $questionnaire->id) }}">
                                <i class="fas fa-notes-medical text-info"> Add One Here</i>
                            </a>
                        </h3>
                    </div> 

                @endforelse

            </div>
          </div>
        </div>

      </div>


@endsection --}}



@extends('admin.layouts.app')


@section('content')

    <div class="row mt-4">
        <div class="col-md-10 offset-md-1">

            @include('admin.inc.errors.backend_errors')
            @include('admin.inc.successes.success')

            <div class="jumbotron shadow border bg-light">
                
                <div class="d-flex justify-content-between">

                    <div class="text-info">
                        <h1 class="display-4 m-0">{{$questionnaire->name}}</h1>
                        <p class="lead">{!! $questionnaire->description !!}</p>
                    </div>

                    <div class="questionnaire-action-container border text-info shadow-sm p-4">
                        <a type="button" data-toggle="collapse" data-target="#questionnaireActionCollapse" aria-expanded="false" aria-controls="questionnaireActionCollapse">
                            Questionnaire options <i class="fas fa-cog"></i>
                        </a>
        
                        <div class="collapse text-right border-bottom" id="questionnaireActionCollapse">

                            @if (!$questionnaire->in_production)
                                <a title="Edit" 
                                class="text-info m-2" 
                                type="button" 
                                href="{{ route('edit_questionnaire', $questionnaire->id) }}"><i class="fas fa-edit"></i>
                                </a>

                                <a title="Delete" 
                                class="text-info m-2" 
                                type="button" 
                                href="#" 
                                data-toggle="modal" 
                                data-target="#deleteModal"><i class="fas fa-trash"></i>
                                </a>  
                            @endif
                            
                            <a title="Preview" 
                                class="text-info m-2" 
                                type="button" 
                                href="{{route('preview_questionnaire', $questionnaire->id)}}"><i class="far fa-share-square"></i>
                            </a>  
                            
                            <a title="{{$questionnaire->in_production ? 'Remove from production' : 'Publish'}}" 
                                class="text-info m-2" 
                                type="button" 
                                href="#"
                                data-toggle="modal"
                                data-target="#setInProductionModal">
                                <i class="fas {{$questionnaire->in_production ? 'fa-lock' : 'fa-lock-open'}}"></i>
                            </a>  
                            
                        </div>

                        <div class="{{$questionnaire->in_production ? "alert alert-info mt-3" : 'd-none'}}" role="alert">
                            <h5 class="alert-heading text-center m-0">
                                {{$questionnaire->in_production ? "Published!" : ''}}
                            </h5>
                        </div>

                    </div>
                </div>

                <hr class="my-4">
                
                <ul class="nav justify-content-around">
                    <li class="nav-item">
                        <a title="Show all questions" class="btn btn-outline-info btn-sm" href="#" class="nav-link" data-toggle="collapse" data-target="#collapseQuestions" aria-expanded="true" aria-controls="collapseQuestions">
                            <i class="far fa-eye"> Questions</i>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a title="Show all responses" class="btn btn-outline-info btn-sm" href="#" class="nav-link" data-toggle="collapse" data-target="#collapseResponses" aria-expanded="true" aria-controls="collapseResponses">
                            <i class="far fa-eye"> Responses</i>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a title="Add new question" class="btn btn-outline-info btn-sm" href="{{route('create_question', $questionnaire->id)}}" class="nav-link">
                            <i class="fas fa-plus-circle"> Question</i>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a title="Add new response" class="btn btn-outline-info btn-sm" href="{{route('create_response', $questionnaire->id)}}" class="nav-link">
                            <i class="fas fa-plus-circle"> Response</i>
                        </a>
                    </li>
                </ul>
                
            </div>
        </div>

        <!-- Modal -->
        @include('admin.inc.modals.delete_modal', [
            'route' => route('delete_questionnaire', $questionnaire->id),
            'deleteModalId' => 'deleteModal',
            'deleteModalLabel' => 'deleteModalLabel',
            'deleteSubject' => $questionnaire->name
            ])

        @include('admin.inc.modals.production_warning_modal', [
            'route' => route('set_questionnaire_in_production', $questionnaire->id),
            'setInProductionModalId' => 'setInProductionModal',
            'setInProductionModalLabel' => 'setInProductionModalLabel',
            'setInProductionSubject' => $questionnaire->name
            ])
    </div>



    <div class="accordion" id="accordionContent">
        <div class="card border-0">
      
          <div id="collapseQuestions" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionContent">
            <div class="card-body mb-4">

                <form action="{{ route('make_questionnaire_schema', $questionnaire->id) }}" method="post">        
                    @csrf

                    <h1 class="text-center text-info">Questions</h1>
                    

                        @forelse ($questions as $question)
                            
                            <div class="card border my-5 shadow">
                                <div class="card-body m-0 p-0">

                                <div class="card-header bg-info text-light d-flex justify-content-between">

                                    <div class="custom-control custom-radio">
                                        <input 
                                        id="{{ $question->id }}"
                                        class="custom-control-input"
                                        type="radio" 
                                        name="starter_question_id[]"
                                        value="{{ $question->id }}"
                                        {{ $question->starter_question == 1 ? 'checked' : '' }}>
                                        
                                        <label for="{{ $question->id }}" class="custom-control-label">Starter Question</label>
                                    </div>

                                    @if (!$questionnaire->in_production)

                                        <div class="question-action-container">
                                            <a type="button" data-toggle="collapse" data-target="#questionActionColapse{{$question->id}}" aria-expanded="false" aria-controls="questionActionColapse{{$question->id}}">
                                                Question options <i class="fas fa-cog"></i>
                                            </a>

                                            <div class="collapse text-right" id="questionActionColapse{{$question->id}}">
                                                
                                                <a class="text-white" href="{{route('edit_question', $question->id)}}">
                                                    <i title="Edit Question" class="fas fa-edit"></i>
                                                </a>
                                                <a class="text-white" href="#" data-toggle="modal" data-target="#deleteModal{{$question->id}}">
                                                    <i title="Delete Question" class="fas fa-trash"></i>
                                                </a>
                                            </div>
                                        </div>

                                    @endif
                                </div>


                                    <div class="text-center p-5"> 
                                        <h4>
                                            <strong>{!! $question->question_response !!}</strong>
                                        </h4>
                                    </div>

                                    <ul class="list-group list-group-horizontal ml-4 mr-4 mb-4">
                                        @foreach ($question->choices as $choice)
                                            <li class="list-group-item border-info list-group-item-action">
                                                <h5 class="text-center border-bottom border-info"> {{$choice->choice}} </h5>
                                                <br>
                                                <small class=""><strong> Jump to: </strong></small>
                                                <div class="form-group">
                                                    <select name="question_choices[{{$choice->id}}][jump_to]" class="custom-select custom-select-md" >
                                            
                                                        <optgroup label="Questions">
                                                            @foreach ($questionnaire->questions_responses as $question_response)
                                                            
                                                                @if ($question_response->type == 'question')
                                                                    <option
                                                                        value="{{ $question_response->id }}"
                                                                        {{ old('question_choices.'. $choice->id .'.jump_to') == $question_response->id ? 'selected' : '' }}
                                                                        {{ $question_response->id == $choice->jump_to ? 'selected' : '' }}>
                                                                        {{Str::limit(strip_tags($question_response->question_response),40)}}
                                                                    </option>
                                                                @endif
                                                        
                                                            @endforeach
                                                        </optgroup>

                                                        <optgroup label="Responses">
                                                            @foreach ($questionnaire->questions_responses as $question_response)
                                                                
                                                                @if ($question_response->type == 'response')
                                                                    <option
                                                                        value="{{ $question_response->id }}"
                                                                        {{ old('question_choices.'. $choice->id .'.jump_to') == $question_response->id ? 'selected' : '' }}
                                                                        {{ $question_response->id == $choice->jump_to ? 'selected' : '' }}>
                                                                        {{Str::limit(strip_tags($question_response->question_response),40)}}
                                                                    </option>
                                                                @endif
                                                            
                                                            @endforeach
                                                        </optgroup>

                                                    </select>
                                                </div>
                                            </li>
                                        @endforeach
                                    </ul>
                                    
                                </div>
                            </div>
        
                            <!-- Modal -->
                            @include('admin.inc.modals.delete_modal', [
                                'route' => route('delete_question', $question->id),
                                'deleteModalId' => 'deleteModal'.$question->id,
                                'deleteModalLabel' => 'deleteModalLabel'.$question->id,
                                'deleteSubject' => $question->question_response
                                ])

                        @empty
                        
                            <div class="text-center text-info w-100 mt-5">
                                <h3>
                                    There are no questions added yet.
                                    <hr class="w-50">
                                    <a href="{{ route('create_question', $questionnaire->id) }}">
                                        <i class="fas fa-notes-medical text-info"> Add One Here</i>
                                    </a>
                                </h3>
                            </div> 
    
                        @endforelse
                        
                        @if ( count($questions) && (!$questionnaire->in_production) )
                            <button class="btn btn-outline-info btn-block" type="submit">Save Questionnaire Schema</button>
                        @endif                            
                </form>

            </div>
          </div>
        </div>

        <div class="card border-0">

          <div id="collapseResponses" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionContent">
            
            <div class="card-body">
                <h1 class="text-center text-info">Responses</h1>

                @forelse ($responses as $response)
                    
                    <div class="card border shadow text-dark mb-3">

                        <div class="card-body">
                            {!! $response->question_response !!}
                        </div>

                        <div class="card-footer text-right p-1">
                            <a href="{{ route('edit_response', $response->id) }}" title="Edit response" class="">
                                <i class="far fa-edit text-info"></i>
                            </a>
                            <a href="{{ route('delete_response', $response->id) }}" title="Delete response" class="ml-2">
                                <i class="fa fa-trash text-info"></i>
                            </a>
                        </div>

                    </div>

                @empty
                
                    <div class="text-center text-info w-100 mt-5">
                        <h3>
                            There are no responses added yet.
                            <hr class="w-50">
                            <a href="{{ route('create_response', $questionnaire->id) }}">
                                <i class="fas fa-notes-medical text-info"> Add One Here</i>
                            </a>
                        </h3>
                    </div> 

                @endforelse

            </div>
          </div>
        </div>

      </div>


@endsection