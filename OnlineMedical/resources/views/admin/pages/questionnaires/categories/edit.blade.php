@extends('admin.layouts.app')

@section('content')

    @include('admin.inc.errors.backend_errors')

    <div class="row min-vh-100 justify-content-center align-items-center">
        <div class="col-md-6">

            @include('admin.inc.successes.success')

            <div class="card shadow">

                <div class="card-header text-info text-center text-uppercase shadow-sm">
                    <h5 class="m-0"> 
                        Edit questionnaire category
                    </h5>
                </div>

                <div class="card-body">
                    <form id="create_category_form" action="{{ route('update_questionnaire_category', $category->id) }}" method="post">
                        @csrf

                        <div class="input-group mb-4">
                            <div class="input-group-prepend">
                                <div class="input-group-text bg-light">
                                    <i class="fab fa-cuttlefish text-info"></i>
                                </div>
                            </div>

                            <label for="blog_title"></label>

                            <input 
                                type="text" 
                                name="category" 
                                value="{{ $category->category }}"
                                placeholder="Enter category" 
                                class="form-control" 
                                aria-describedby="category_help">

                            <div class="error-container w-100"></div>

                        </div>

                            <div class="form-group">
                                <div id="accordion">
                                    
                                    <div class="card border" id="icon_container">
                                    
                                        <div class="card-header p-0" id="headingOne">
                                            <a 
                                                class="btn btn-link"
                                                data-toggle="collapse" 
                                                data-target="#collapseOne" 
                                                aria-expanded="false" 
                                                aria-controls="collapseOne">
                                                Choose an icon that will be displayed on all questionaires under this category

                                                {{-- <div class="text-info"> --}}
                                                    {{-- <hr class="m-0"> --}}
                                                    <h4 class="pull-left text-info"> &#8597;</h4>
                                                    {{-- <span class="text-center">Click to toggle</span> --}}
                                                    <h4 class="pull-right text-info"> &#8597;</h4>
                                                {{-- </div> --}}
                                            </a>    
                                        </div>
                                
                                        <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                                            <div class="card-body">
                                                <div class="row justify-content-between">

                                                    @foreach ($icons as $icon)
                                                        <div class="col-md-3 text-center">

                                                            <input 
                                                                name="icon_id"
                                                                type="radio" 
                                                                value="{{$icon->id}}"
                                                                class="d-none" 
                                                                id="icon_{{$icon->id}}" 
                                                                {{$category->icon_id == $icon->id ? 'checked' : ''}}>
                                                            
                                                            <label for="icon_{{$icon->id}}" class="w-100">
                                                                <div class="card border border-info">
                                                                    <div class="card-body hvr-grow p-2 cursor-pointer">
                                                                        <h3 class="text-info m-0">
                                                                            {!! $icon->icon !!}
                                                                        </h3>
                                                                    </div>
                                                                </div>
                                                            </label>
                                                            
                                                        </div>
                                                    @endforeach

                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="error-container"></div>

                                </div>
                            </div>

                        <button type="submit" class="btn btn-outline-info btn-block mb-3">Save</button>

                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('blade_scripts')
    <script src="{{ asset('admin/js/questionnaire/category/categories.js') }}" defer></script>
@endsection