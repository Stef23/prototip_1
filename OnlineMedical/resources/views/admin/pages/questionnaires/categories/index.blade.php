@extends('admin.layouts.app')

@section('content')

    <div class="row mt-3">
        <div class="col-md-6 offset-md-3">

            @include('admin.inc.errors.backend_errors')
            @include('admin.inc.successes.success')

            <div class="jumbotron text-center text-info text-uppercase shadow-sm my-3">
                <h3 class="m-0">Questionnaire categories</h3>
            </div>

            <ul class="list-group">
                @forelse ($categories as $category)
                    <li class="list-group-item d-flex justify-content-between mb-2 border shadow-sm">

                        <div class="d-flex flex-column">
                            <p class="pull-left mb-1 text-info">{{ $category->category }}</p>

                            <small>  
                                This category has 
                                <strong class="text-info">
                                    ( {{ $category->questionnaires_count }} )
                                </strong> clinics under it.
                            </small>
                        </div>

                        <div class="d-flex align-items-center">
                            <a class="" title="Edit Category" href="{{ route('edit_questionnaire_category', $category->id) }}">
                                <i class="far fa-edit text-warning"></i>
                            </a>
                            <a class="ml-2"  title="Delete Category" href="{{ route('delete_questionnaire_category', $category->id) }}">
                                <i class="fa fa-trash-alt text-danger"></i>
                            </a>
                        </div>

                    </li>

                @empty

                    {{-- <li class="list-group-item">No categories added / <a href="{{ route('create_questionnaire_category') }}">Add Categories</a> </li>                     --}}
                
                    <div class="text-center text-info w-100 mt-5">
                        <h3>
                            There is no categories added yet.
                            <hr class="w-50">
                            <a href="{{ route('create_questionnaire_category') }}">
                                <i class="fas fa-notes-medical text-info"> Add One Here</i>
                            </a>
                        </h3>
                    </div> 

                @endforelse
            </ul>

        </div>
    </div>
    
@endsection