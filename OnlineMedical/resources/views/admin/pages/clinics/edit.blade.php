@extends('admin.layouts.app')


@section('content')

<div class="row text-center my-5">
    <div class="col-md-6 offset-md-3">

        @include('admin.inc.errors.backend_errors')

        <div class="card shadow">

            <div class="card-header text-info text-center text-uppercase shadow-sm">
                <h4 class="m-0">Edit Clinic</h4>
            </div>

            <div class="card-body">
                <form id="create_clinic_form" action="{{ route('update_clinic', $clinic->id) }}" method="post" enctype="multipart/form-data">
                    @csrf

                    <div class="input-group">
                        <label class="btn btn-light border btn-block text-muted mb-4" for="clinic_image">
                            Change the image of your clinic
                            <input 
                                type="file" 
                                name="image" 
                                class="clinic-file-input d-none" 
                                id="clinic_image" 
                                aria-describedby="inputGroupFileImage">
                        </label>
                    </div>

                    <div class="input-group mb-4">
                        <div class="input-group-prepend">
                            <div class="input-group-text bg-light">
                                <i class="fas fa-ankh text-info"></i>
                            </div>
                        </div>

                        <label for="clinic_name"></label>

                        <input 
                            type="text" 
                            name="name" 
                            value="{{ $clinic->name }}" 
                            id="clinic_name" 
                            class="form-control" 
                            autocomplete="off" 
                            placeholder="Provide a clinic name" 
                            aria-describedby="name_help">
                    </div>

                    <div class="input-group mb-4">
                        <div class="input-group-prepend">
                            <div class="input-group-text bg-light p-2">
                                <i class="fas fa-phone-alt text-info"></i>
                            </div>
                        </div>

                        <label for="clinic_telephone_number"></label>
                        
                        <input 
                            type="tel" 
                            name="telephone_number" 
                            value="{{ $clinic->telephone_number }}" 
                            id="clinic_telephone_number" 
                            class="form-control" 
                            autocomplete="off" 
                            placeholder="Provide a clinic telephone number" 
                            aria-describedby="telephone_number_help">
                    </div>

                    <div class="input-group mb-4">
                        <div class="input-group-prepend">
                            <div class="input-group-text bg-light p-2">
                                <i class="fas fa-pencil-alt text-info"></i>
                            </div>
                        </div>

                        <label for="clinic_description"></label>
                        
                        <textarea 
                            name="description" 
                            id="clinic_description" 
                            class="form-control" 
                            cols="30" 
                            rows="4" 
                            placeholder="Provide a description for your clinic" 
                            aria-describedby="description_help">{{ $clinic->description }}</textarea>
                    </div>

                    <div class="input-group mb-4">
                        <div class="input-group-prepend">
                            <div class="input-group-text bg-light p-2">
                                <i class="fas fa-map-marker-alt text-info"></i>
                            </div>
                        </div>

                        <label for="clinic_location"></label>

                        <input 
                            type="text" 
                            name="location" 
                            value="{{ $clinic->location }}" 
                            id="clinic_location" 
                            class="form-control" 
                            autocomplete="off" 
                            placeholder="Provide a clinic location" 
                            aria-describedby="clinic_location_help">
                    </div>

                    <div class="input-group mb-4">
                        <div class="input-group-prepend">
                            <label class="input-group-text bg-light" for="category">
                                <i class="fas fa-sort text-info"></i>
                            </label>
                        </div>
                        
                        <label for="clinic_category"></label>
                        
                        <select name="c_category_id" id="clinic_category" class="form-control">
                            @forelse ($categories as $category)                                
                                    <option 
                                        value="{{ $category->id }}" 
                                        {{ ($category->id == $clinic->c_category_id) ? 'selected' : '' }}>
                                        {{$category->category}}
                                    </option>
                                @empty
                                    <option disabled value="">No categories added</option>                                
                                @endforelse
                        </select>
                    </div>

                    <button class="btn btn-outline-info btn-block">Store</button>

                </form>
            </div>
        </div>
    </div>
</div>

@endsection

@section('blade_scripts')
    <script src="{{ asset('admin/js/clinics/clinics.js') }}" defer crossorigin="anonymous"></script>
@endsection