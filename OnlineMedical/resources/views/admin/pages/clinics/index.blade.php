@extends('admin.layouts.app')

@section('blade_css')
    <link href="{{ asset('admin/css/clinics/index.css') }}" rel="stylesheet">    
@endsection

@section('content')

<div id="our_clinics" class="row">

    <div class="col-md-10 offset-md-1">

        @include('admin.inc.successes.success')

        <div class="row"> 

            @forelse ($clinics as $clinic)
                <div class="col-md-4 my-3">
                    <div class="card p-0 shadow">

                        <div class="card-body p-0 cursor-pointer" data-toggle="modal" data-target="#clinicLocation{{ $clinic->id }}">
                            <img class="img-fluid" src="{{ asset('storage/'.$clinic->image) }}" alt="">
                            
                            <div class="p-3">
                                <h5 class="my-2">{{ $clinic->name }}</h5>
                                <p>{{ $clinic->description }}</p>
                                <small> Category: {{ $clinic->c_categories->category }}</small>
                                <br>
                                <small> Contact: {{ $clinic->telephone_number }}</small>
                            </div>
                        </div>

                        <div class="card-action p-3 d-flex justify-content-between">
                            <a href="{{ route('edit_clinic', $clinic->id) }}" class="">
                                <i class="far fa-edit text-warning"></i>
                            </a>
                            <a href="{{ route('delete_clinic', $clinic->id) }}" class="">
                                <i class="fas fa-trash-alt text-danger"></i>
                            </a>
                        </div>

                    </div>   
                </div>

                <div id="clinicLocation{{ $clinic->id }}" class="modal fade" role="dialog">
                    <div class="modal-dialog location-modal-body modal-dialog-centered modal-dialog-scrollable">
                        
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">Location</h4>
                            </div>

                            <div class="modal-body">
                                {!! $clinic->location !!}
                            </div>
                            
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default btn-outline-info" data-dismiss="modal">Close</button>
                            </div>
                        </div>

                    </div>
                </div>
                
            @empty

                <div class="text-center text-info w-100 mt-5">
                    <h3>
                        There are no clinics added yet.
                        <hr class="w-50">
                        <a href="{{ route('create_clinic') }}">
                            <i class="fas fa-notes-medical text-info"> Add One Here</i>
                        </a>
                    </h3>
                </div>  
                
            @endforelse

        </div>
    </div>
</div>

@endsection