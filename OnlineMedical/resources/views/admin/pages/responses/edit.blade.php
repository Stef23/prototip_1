@extends('admin.layouts.app')

@section('content')

    <div class="pull-right mt-3">
        <a href="{{ route('show_questionnaire', $response->questionnaire->id) }}">
            <i class="far fa-arrow-alt-circle-left text-info display-4"></i>
        </a>
    </div>


    <div class="row mt-3">
        <div class="col-md-6 offset-md-3">

            @include('admin.inc.errors.backend_errors')
            @include('admin.inc.successes.success')

            <form id="create_response_form" action="{{ route('update_response', $response->id) }}" method="post">
                @csrf
                
                <div class="form-group">
                    <label for="questionnaire_contents">Edit Response</label>
                    <textarea
                          name="questionnaire_contents[question_response]"
                          id="questionnaire_contents"
                          class="form-control"
                          cols="10"
                          rows="2"
                          aria-describedby="questionnaire_contents_help">{!! $response->question_response !!}</textarea>

                    <input 
                        type="hidden" 
                        name="questionnaire_contents[type]" 
                        value="response">

                    <small id="questionnaire_contents_help" class="form-text text-muted">
                        Provide a response for the <strong>{{ $response->questionnaire->name }}</strong> questionnaire.
                    </small>
                </div>

                <button id="save_response" type="submit" class="btn btn-block btn-outline-info mt-4">Save Response</button>
  
              </form>
        </div>
    </div>
@endsection

@section('blade_scripts')
    <script src="{{asset('admin/js/responses/responses.js')}}"></script>
@endsection