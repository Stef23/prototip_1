<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>



    <!-- Fonts -->
    @include('admin.inc.fonts')

    <!-- Styles -->
    @include('admin.inc.styles')

</head>
<body>
    <div id="app">
        <main class="wrapper d-flex align-items-stretch">
            @if ( !(\Request::is('login')   || \Request::is('register')) )
                @include('admin.inc.sidenav')                
            @endif

            <div id="content" class="container-fluid ">
                @yield('content')
            </div>
        </main>
    </div>
    <!-- Scripts -->
    @include('admin.inc.scripts')
</body>

</html>
