<div class="modal fade" id="{{$deleteModalId}}" tabindex="-1" role="dialog" aria-labelledby="{{$deleteModalLabel}}" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">

            <div class="modal-header">
                <h1 class="modal-title text-danger" id="{{$deleteModalLabel}}">Warning!</h1>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body">
                <h3>Are you shure you wan't to <strong>delete</strong> {{strip_tags($deleteSubject)}}?</h3>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">No, cancel</button>
                <a href="{{$route}}" type="button" class="btn btn-danger text-white">Yes, delete</a>
            </div>

        </div>
    </div>
</div>