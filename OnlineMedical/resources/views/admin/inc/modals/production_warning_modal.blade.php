<div class="modal fade" id="{{$setInProductionModalId}}" tabindex="-1" role="dialog" aria-labelledby="{{$setInProductionModalLabel}}" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">

            <div class="modal-header">
                <h1 class="modal-title text-danger" id="{{$setInProductionModalLabel}}">Warning!</h1>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body">
                <h3>Are you shure you wish to change the state of the {{strip_tags($setInProductionSubject)}} questionnaire?</h3>
                <small>Please carefully review the questionnaire before agreen</small>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">No, cancel</button>
                <a href="{{$route}}" type="button" class="btn btn-danger text-white">Yes</a>
            </div>

        </div>
    </div>
</div>