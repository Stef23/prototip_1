<nav id="sidebar">
    <div class="custom-menu">
        <button type="button" id="sidebarCollapse" class="btn btn-primary">
        <i class="fa fa-bars"></i>
        <span class="sr-only">Toggle Menu</span>
        </button>
    </div>
<div class="p-4 pt-5">
    
    <h1><a href="{{route('home')}}" class="logo">Home</a></h1>

    <ul class="list-unstyled components mb-5">
        <li class="active">
            <a href="#questionnaireSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                <i class="fas fa-question"></i> 
                <i>Questionnaires</i>
            </a>
            <ul class="collapse list-unstyled" id="questionnaireSubmenu">
                <li>
                    <a href="{{route('show_all_questionnaires')}}">
                        <i class="fas fa-notes-medical"> All</i>
                    </a>
                </li>
                <li>
                    <a href="{{route('create_questionnaire')}}">
                        <i class="fas fa-plus-circle"> Create</i>
                    </a>
                </li>
                <li>
                    <a href="#categoriesSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                        <i>Categories</i>
                    </a>
                    <ul class="collapse list-unstyled" id="categoriesSubmenu">
                        <li>
                            <a href="{{route('show_all_questionnaire_categories')}}">
                                <i class="fas fa-notes-medical"> All</i>
                            </a>
                        </li>
                        <li>
                            <a href="{{route('create_questionnaire_category')}}">
                                <i class="fas fa-plus-circle"> Create</i>
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </li>

        <li>
            <a href="#blogsSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                <i class="fab fa-blogger"></i> 
                <i>Blogs</i>
            </a>
            <ul class="collapse list-unstyled" id="blogsSubmenu">
                <li>
                    <a href="{{route('show_all_blogs')}}">
                        <i class="fas fa-notes-medical"> All</i>
                    </a>
                </li>
                <li>
                    <a href="{{route('get_blog_main_category', 'blog')}}">
                        <i class="fas fa-plus-circle"> Create</i>
                    </a>
                </li>
                <li>
                    <a href="#b_categoriesSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                        <i>Categories</i>
                    </a>
                    <ul class="collapse list-unstyled" id="b_categoriesSubmenu">
                        <li>
                            <a href="{{route('show_all_blog_categories')}}">
                                <i class="fas fa-notes-medical"> All</i>
                            </a>
                        </li>
                        <li>
                            <a href="{{route('create_blog_category')}}">
                                <i class="fas fa-plus-circle"> Create</i>
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('get_blog_main_category', 'sub_category') }}">
                                <i class="fas fa-plus-circle"> Create Sub Category</i>
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </li>
        
        <li>
            <a href="#clinicsSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                <i class="fas fa-clinic-medical"></i> 
                <i>Clinics</i>
            </a>
            <ul class="collapse list-unstyled" id="clinicsSubmenu">
                <li>
                    <a href="{{ route('show_all_clinics') }}">
                        <i class="fas fa-notes-medical"> All</i>
                    </a>
                </li>
                <li>
                    <a href="{{ route('create_clinic') }}">
                        <i class="fas fa-plus-circle"> Create</i>
                    </a>
                </li>
                <li>
                    <a href="#c_categoriesSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                        <i>Categories</i>
                    </a>
                    <ul class="collapse list-unstyled" id="c_categoriesSubmenu">
                        <li>
                            <a href="{{ route('show_all_clinic_categories') }}">
                                <i class="fas fa-notes-medical"> All</i>
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('create_clinic_category') }}">
                                <i class="fas fa-plus-circle"> Create</i>
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </li>

        <li>
            <a href="#productsSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                <i class="fab fa-product-hunt"></i> 
                <i> Products</i>
            </a>
            <ul class="collapse list-unstyled" id="productsSubmenu">
                <li>
                    <a href="{{ route('show_all_products') }}">
                        <i class="fas fa-notes-medical"> All</i>
                    </a>
                </li>
                <li>
                    <a href="{{ route('get_product_main_category', 'product') }}">
                        <i class="fas fa-plus-circle"> Create</i>
                    </a>
                </li>
                <li>
                    <a href="#p_categoriesSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                        <i>Categories</i>
                    </a>
                    <ul class="collapse list-unstyled" id="p_categoriesSubmenu">
                        <li>
                            <a href="{{ route('show_all_product_categories') }}">
                                <i class="fas fa-notes-medical"> All</i>
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('create_product_category') }}">
                                <i class="fas fa-plus-circle"> Create Category</i>
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('get_product_main_category', 'sub_category') }}">
                                <i class="fas fa-plus-circle"> Create Sub Category</i>
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </li>
    </ul>

    <ul class="list-unstyled">
        <li>
            <a class="" href="{{ route('logout') }}"
            onclick="event.preventDefault();
                          document.getElementById('logout-form').submit();">
             <i class="fas fa-sign-out-alt"> {{ __('Logout') }}</i>
         </a>

         <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
             @csrf
         </form>
        </li>
    </ul>

    {{-- <div class="sidenav-footer">
        <p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="icon-heart" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib.com</a>
                <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
    </div> --}}

</div>
</nav>