@extends('admin.layouts.app')

@section('content')
<div class="">
    <div class="row my-4 text-center">
        <div class="col-md-6 offset-md-3 p-4 shadow border">
            <div class=" border-info text-info">
                <h3>Welcome {{ Auth::user()->name }}</h3>
                <p>Lets do some great things today!</p>
            </div>
        </div>
    </div>

    <div class="row text-center mb-5">
        <a href="{{ route('show_all_blogs') }}" class="col-md-4 btn">
            <div class="p-4 shadow text-info border">
                <h3>Blogs</h3>
            </div>
        </a>

        <a href="{{ route('show_all_clinics') }}" class="col-md-4 btn">
            <div class="p-4 shadow text-info border">
                <h3>Clinics</h3>
            </div>
        </a>

        <a href="{{ route('show_all_products') }}" class="col-md-4 btn">
            <div class="p-4 shadow text-info border">
                <h3>Products</h3>
            </div>
        </a>
    </div>

    <div class="row">
        <div class="col-md-12">
            <h4 class="text-center text-uppercase text-info p-3">Published Questionnaires</h4>
                
                <table class="table table-bordered table-sm text-center text-muted">

                    <thead>
                        <tr>
                            <th>Title</th>
                            <th>Category</th>
                            <th>Description</th>
                            <th>Created at</th>
                            <th>Action</th>        
                        </tr>
                    </thead>

                    <tbody>
                        @forelse ($allQuestionnaires as $questionnaire)
                            <tr>
                                <td>{{ $questionnaire->name }}</td>
                                <td>{{ $questionnaire->category->category }}</td>
                                <td>{!! $questionnaire->description !!}</td>
                                <td>{{ $questionnaire->created_at }}</td>
                                <td>
                                    <a href="{{ route('show_questionnaire', $questionnaire->id) }}" class="">
                                        <i class="far fa-eye text-info"></i>
                                    </a>
                                </td>
                            </tr>

                        @empty


                        @endforelse
                        
                    </tbody>
                </table>

                <div class="bg-white">
                    <a href="{{route('show_all_questionnaires')}}" class="btn btn-block btn-outline-info btn-sm">Show all</a>
                </div>
                
            {{-- </div> --}}
        </div>



    </div>

    <div class="row">
        
    </div>
    
</div>

@forelse ($allQuestionnaires as $questionnaire)
     <!-- Modal -->
     <div class="modal fade" id="deleteModal{{$questionnaire->id}}" tabindex="-1" role="dialog" aria-labelledby="deleteModal{{$questionnaire->id}}Label" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">

                <div class="modal-header">
                    <h1 class="modal-title text-danger" id="deleteModal{{$questionnaire->id}}Label">Warning!</h1>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <h3>Are you shure you wan't to <strong>delete</strong> this questionnaire?</h3>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">No, cancel</button>
                    <a href="{{route('delete_questionnaire', $questionnaire->id)}}" type="button" class="btn btn-secondary text-white">Yes, delete</a>
                </div>
                
            </div>
        </div>
    </div>
@empty

    <div class="text-center text-info w-100 mt-5">
        <h3>
            There are no published questionnaires yet.
            <hr class="w-50">
            <a href="{{ route('create_questionnaire') }}">
                <i class="fas fa-notes-medical text-info"> Add One Here</i>
            </a>
        </h3>
    </div>

@endforelse



@endsection
