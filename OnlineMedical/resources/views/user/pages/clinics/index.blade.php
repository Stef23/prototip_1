@extends('user.layouts.app')

@section('content')
<div class="col-12 bg-light border rounded-bottom">
    @include('user.inc.navbars.category_navbar')
</div>
<div id="our_clinics" class="row cover-bg min-vh-100" style="background-image: url({{asset('user/imgs/questionnaire/background.svg')}});">

    <div class="col-md-10 offset-md-1">
        <div class="row"> 

            @forelse ($clinics as $clinic)
                <div class="col-lg-3 col-md-6 my-3">
                    <div class="card p-0 shadow">

                        <div class="card-body p-0 cursor-pointer custom-hvr-img-zoom" data-toggle="modal" data-target="#clinicLocation{{ $clinic->id }}">
                            <img class="img-fluid" src="{{ asset('storage/'.$clinic->image) }}" alt="">
                            <div class="p-3">
                                <h5 class="my-2">{{ $clinic->name }}</h5>
                                <p>{{ $clinic->description }}</p>
                                <small> Категорија: {{ $clinic->c_categories->category }}</small>
                                <br>
                                <small> Контакт: {{ $clinic->telephone_number }}</small>
                            </div>
                        </div>

                    </div>   
                </div>

                <div id="clinicLocation{{ $clinic->id }}" class="modal fade" role="dialog">
                    <div class="modal-dialog location-modal-body modal-dialog-centered modal-dialog-scrollable">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">Location</h4>
                            </div>
                            <div class="modal-body">
                                {!! $clinic->location !!}
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default btn-outline-info" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
                
            @empty

                <div class="text-center text-info w-100 mt-5">
                    <h3>
                        There are no clinics added yet.
                    </h3>
                </div>  
                
            @endforelse

        </div>
    </div>
</div>
@endsection