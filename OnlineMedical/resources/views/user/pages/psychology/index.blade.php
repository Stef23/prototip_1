@extends('user.layouts.app')

@section('blade_css')
    <link rel="stylesheet" href="{{asset('user/css/fitness/index.css')}}">
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12 p-0" style="height:75vh">
            <div id="carouselFitness" class="carousel slide h-100" data-ride="carousel">
                
                <ol class="carousel-indicators">
                    <li data-target="#carouselFitness" data-slide-to="0" class="active"></li>
                    <li data-target="#carouselFitness" data-slide-to="1"></li>
                    <li data-target="#carouselFitness" data-slide-to="2"></li>
                  </ol>
                
                
                <div class="carousel-inner h-100">
                  @foreach ($blogs->take(3) as $blog)
                    

                        <div class="carousel-item {{ $loop->first ? 'active' : ''}} h-100">
                            {{-- <img src="{{asset('storage/'.$blog->cover_image)}}" class="d-block w-100" alt="..."> --}}
                            <img src="{{$blog->cover_image}}" class="d-block w-100" alt="...">
                            <div class="carousel-caption mb-4 d-none d-md-block">
                                <h2 class="text-white mb-5"> <strong> {{$blog->title}} </strong> </h2>
                                <a href="{{route('user.show_blog', $blog->id)}}" class="btn btn-primary">Повеќе</a>
                            </div>
                        </div>
                    
                                          
                  @endforeach
                </div>


                <a class="carousel-control-prev" href="#carouselFitness" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselFitness" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>

            </div>
        </div>
    </div>
    <div class="">
        @include('user.inc.navbars.sub_category_navbar')
    </div>
    <div class="row">
        <div class="col-md-7 offset-md-1">
            <div class="row no-gutters">
                @foreach ($blogs->skip(3) as $blog)
                    
                    <div class="col-12">
                        <div class="card border-0 mb-3">
                            <div class="row no-gutters align-items-center">
                                <div class="col-md-4">
                                    {{-- <img src="{{asset('storage/'.$blog->cover_image)}}" class="card-img" alt="..."> --}}
                                    <img src="{{$blog->cover_image}}" class="card-img" alt="...">
                                </div>
                                <div class="col-md-8">
                                    <div class="card-body">
                                        <h5 class="card-title">{{$blog->title}}</h5>
                                        <p class="card-text">{{Str::limit(strip_tags($blog->body), 100)}}
                                            <br>
                                                <a href="{{route('user.show_blog', $blog->id)}}" class="text-decoration-none">
                                                <small class="badge badge-dark mt-4">Прочитај повеќе</small>
                                            </a>
                                        </p>
                                        <p class="card-text">
                                            <small class="text-muted">Last updated 3 mins ago</small>
                                            <small class="text-muted pull-right mr-2">{{$blog->sub_category->sub_category}}</small>
                                        </p>   
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                
                @endforeach

            </div>

        </div>
        <div class="col-md-3">
            <div class="row no-gutters">
                @for ($i = 0; $i < 2; $i++)
                <div class="col-12">
                    <div class="card bg-dark text-white mb-3">
                        <img src="https://image.freepik.com/free-photo/awesome-bodybuilder-silhouette-handsome-power-athletic-man-bodybuilder-fitness-muscular-body-dark-colour-smoke-background-perfect-male-tattoo-posing_136403-1997.jpg" class="card-img" alt="...">
                        <div class="card-img-overlay">
                          <h5 class="card-title">Card title</h5>
                          <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                          <p class="card-text">Last updated 3 mins ago</p>
                        </div>
                    </div>
                </div>
                @endfor
            </div>
        </div>
        <div class="col-md-12 mt-5">
            {{$blogs->withQueryString()->links()}}
        </div>
    </div>
            
@endsection