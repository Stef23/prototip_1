@extends('user.layouts.app')

@section('blade_css')
    <link href="{{ asset('user/css/blogs/index.css') }}" rel="stylesheet">    
@endsection

@section('content')

            
    @forelse ($blogs as $blog)
       
    
        @if($loop->first)


        <div class="row animated fadeIn">
            <div class="col-12 p-0">
                @include('user.inc.navbars.sub_category_navbar')
            </div>
            <div class="col-md-8 offset-md-2 my-3">
                <p class="custom-tablet-screen-p-font">Последно објавено:</p>
                <a href="{{route('user.show_blog', $blog->id)}}" class="text-decoration-none text-dark">
                    <div id="main_card" class="card border-0 shadow">
                        <div class="card-body min-vh-50 p-0 cover-bg hvr-shadow" 
                        {{-- style="background-image: url({{asset('storage/'.$blog->cover_image)}});" --}}
                        style="background-image: url({{asset($blog->cover_image)}});"
                            >
                        </div>
                        <div id="main_card_footer" class="card-footer px-0 py-4 shadow">
                            <p class="custom-tablet-screen-p-font h5 border-left border-primary pl-2"><strong>{{$blog->title}}</strong></p>
                            <p class="custom-tablet-screen-p-font px-2">{{Str::limit(strip_tags($blog->body), 80)}}</p>
                            <small class="boder border-bottom border-primary text-info">Прочитај повеќе <i class="text-primary  ml-2 fas fa-arrow-circle-right "></i> </small>
                        </div>
                        <div class="px-3">
                            {{-- <small class="pull-left">Од: {{$blog->user->name}}</small> --}}
                            {{-- <small class="pull-right">Објавено на: {{$blog->created_at->toDateString()}}</small> --}}
                        </div>

                    </div>
                </a>
            </div>
        </div>

        <div class="row bg-light">
            <div class="col-md-10 offset-md-1">
                <p class="division custom-tablet-screen-p-font">Останати блогови</p>

                <div class="row animated fadeIn">
            
            
            @else
            
            
                    <div class="col-md-4 my-3 hvr-shadow">
                        <a href="{{route('user.show_blog', $blog->id)}}" class="text-decoration-none text-dark">
                            <div class="card border-0 shadow">
                                <div class="card-body min-vh-25 p-0 cover-bg" 
                                    {{-- style="background-image: url({{asset('storage/'.$blog->cover_image)}});" --}}
                                    style="background-image: url({{asset($blog->cover_image)}});"
                                    >
                                </div>
                                <div class="card-footer bg-white py-4">
                                    <p class="h5 text-center"><strong>{{$blog->title}}</strong></p>
                                </div>
                                <div>
                                    <p class="card-text"><small class="text-primary badge badge-dark d-block">{{$blog->sub_category->sub_category}}</small></p>

                                </div>
                            </div>
                        </a>
                    </div>


            @endif
            
            @empty
            
                <div class="jumbotron"><h1>No posts added yet</h1></div>
            
            @endforelse
                
                
                </div>
                {{-- <p class="division">Најчитани блогови</p> --}}
            </div>
            <div class="col-md-12 mt-5">
                {{$blogs->withQueryString()->links()}}
            </div>

        </div>

@endsection