@extends('user.layouts.app')

@section('blade_css')
    <link href="{{ asset('user/css/blogs/show.css') }}" rel="stylesheet">    
@endsection

@section('content')
    <div class="">
        {{-- <div class="row min-vh-100 cover-bg" style="background-image: url({{asset('storage/'.$blog->cover_image)}});"></div> --}}
        <div class="row min-vh-100 cover-bg" style="background-image: url({{asset($blog->cover_image)}});"></div>
        <div id="blog-main-content-container" class="row">
            <div id="blog-title" class="border-bottom border-primary col-lg-6 offset-lg-3 col-md-8 offset-md-2 min-vh-50 shadow-lg d-flex align-items-center justify-content-center">
                <h1 class="text-center custom-tablet-screen-heading-font"> <strong> {{$blog->title}} </strong> </h1>
            </div>
            <div id="blog-body" class="col-lg-6 offset-lg-3 col-md-8 offset-md-2">
                {!! $blog->body !!}
            </div>
        </div>
        <div class="custom-hr"></div>
        <div class="row p-5">
            <div class="col-md-8 offset-md-2">
                <h1 class="text-center custom-tablet-screen-heading-font">Споделете:</h1>
                <p class="text-center h2 text-primary ">
                    <i class="fab fa-facebook-square"></i>
                    <i class="fab fa-linkedin"></i>
                </p>
            </div>
        </div>
        <div class="row p-5 min-vh-50 bg-light justify-content-center align-items-center">
            <div class="col-md-12 text-center">
                <h3 class="custom-tablet-screen-heading-font">Можеби ќе ве интересира:</h3>
                <div class="custom-hr"></div>
            </div>
            @forelse ($relatedBlogs as $blog)
            <div class="col-md-6 col-lg-3 my-3 hvr-shrink">
                <a href="{{route('user.show_blog', $blog->id)}}" class="text-decoration-none text-dark">
                    <div class="card border-0 shadow">
                        <div class="card-body min-vh-25 p-0 cover-bg" 
                        {{-- style="background-image: url({{asset('storage/'.$blog->cover_image)}});" --}}
                        style="background-image: url({{asset($blog->cover_image)}});"
                             >
                        </div>
                        <div class="card-footer bg-white py-4">
                            <p class="h5 text-center"><strong>{{$blog->title}}</strong></p>
                        </div>
                    </div>
                </a>
            </div>
            @empty
                
            @endforelse
        </div>
    </div>
    
@endsection