@extends('user.layouts.app')
@section('blade_css')

<link href="{{ asset('user/css/questionnaires/show.css') }}" rel="stylesheet">
    
@endsection

@section('content')

        <div data="{{$questionnaire->id}}" 
                class="row min-vh-100 flex-column justify-content-center animated fadeIn cover-bg" 
                id="questionnaire_content"
                style="    background-image: url({{asset('user/imgs/questionnaire/background.svg')}});">
        
        </div>


@section('questionnaire_js')

        <script src="{{ asset('user/js/questionnaire.js') }}" defer></script>

@endsection

@endsection