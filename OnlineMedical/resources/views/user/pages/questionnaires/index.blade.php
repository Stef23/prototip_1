@extends('user.layouts.app')
@section('blade_css')

<link href="{{ asset('user/css/questionnaires/index.css') }}" rel="stylesheet">
    
@endsection

@section('content')

<div class="row min-vh-100 animated fadeIn pt-4 cover-bg" style="background-image: url({{asset('user/imgs/questionnaire/background.svg')}});">
    <div class="col-lg-6 offset-lg-3 col-md-8 offset-md-2 py-4">
        <h4 class="text-center my-4 animated fadeInDown">Добредојдовте, јас сум Вашиот Персонален медицински асистент, може да Ви помогнам со:</h4>
        @forelse ($questionnaires as $questionnaire)
            <div class="jumbotron border border-primary text-dark bg-white shadow">
                <div class="row flex-row align-items-center justify-content-around">
                    <div class="col-md-8 questionnaire-card-body">
                        <h3 class="custom-tablet-screen-heading-font">{{$questionnaire->name}}</h3>
                        <p class="custom-tablet-screen-p-font">{!! $questionnaire->description !!}</p>
                        <hr>
                        <a href="{{route('user.show_questionnaire', $questionnaire->id)}}" class="btn btn-primary">Пополни го прашалникот</a>        
                    </div>
                    <div class="col-md-4 questionnaire-card-footer text-right">
                        <p class="display-3 text-primary ">{!!$questionnaire->category->icon->icon!!}</p>
                        <h5 class="text-primary pr-2">{!!$questionnaire->category->category!!}</h5>
                    </div>
                </div>
            </div>
        @empty
            
        @endforelse    
    </div>        
</div>
@endsection