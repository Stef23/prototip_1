@extends('user.layouts.app')

@section('content')
<div class="row min-vh-100 cover-bg" style="background-image: url({{asset('user/imgs/questionnaire/background.svg')}});">
    <div class="col-md-10 offset-md-1">
        <div class="row">
            @forelse ($products as $product)
                <div class="col-lg-3 col-md-6 my-3">
                    <a href="" class="text-decoration-none" data-toggle="modal" data-target="#myDescriptionModal{{ $product->id }}">
                        <div class="card p-0 shadow">
                            <div class="card-body p-0 cursor-pointer custom-hvr-img-zoom">
                                <img class="img-fluid" src="{{ asset('storage/'.$product->image) }}" alt="">
                            </div>
                            <div class="p-3">
                                <h5 class="my-2">{{ $product->name }}</h5>
                                <p>{{ $product->short_description }}</p>
                                <br>
                                <small class="">Категории: {{ $product->p_sub_category->sub_category }}</small>
                            </div>

                        </div>
                    </a>   
                </div>

                <div id="myDescriptionModal{{ $product->id }}" class="modal fade" role="dialog">
                    <div class="modal-dialog modal-md modal-dialog-centered modal-dialog-scrollable">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">Опис</h4>
                            </div>
                            <div class="modal-body">
                                {!! $product->description !!}
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default btn-outline-primary" data-dismiss="modal">Затвори</button>
                            </div>
                        </div>
                    </div>
                </div>
                
            @empty
                <h3>No products added</h3>
            @endforelse

        </div>
    </div>
</div>
@include('user.inc.footers.footer')

@endsection