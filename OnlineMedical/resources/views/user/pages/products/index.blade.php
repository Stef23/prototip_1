@extends('user.layouts.app')

@section('blade_css')
    <link href="{{ asset('user/css/products/index.css') }}" rel="stylesheet">    
@endsection


@section('content')

  <div id="product_page_heading" class="row min-vh-100 cover-bg" style="background-image: url({{asset('user/imgs/products/4.jpg')}});">
    
    <div id="product_page_heading_container" class="col-md-12 p-4 text-center d-flex flex-column justify-content-center">
      <div>
        <h3>Добредојдовте на нашата палета на производи</h3>
        <p>Овде за секој производ може да најдете детален опис и упатство за употреба.</p>
        <button class="category-btn btn btn-lg btn-primary">Категории</button>
      </div>

    </div>

    <div class="overlay"></div>
    
    <div id="product_category_nav" class="col-md-12 min-vh-100 d-flex flex-column justify-content-center">
      
      <a class="btn category-btn">
        <i class="fas fa-times"></i>
      </a>

      <div class="row justify-content-center align-items-center">
      @forelse ($categories as $category)                
          <div class="col-lg-3 col-md-6 ">

            <div class="card mb-3 text-center bg-transparent">
                <div class="text-primary">
                  <h5> <strong> {{ $category->category }} </strong> </h5>
                </div>

                <div class="card-body p-1">
                    <ul class="list-group">
                    @forelse ($category->sub_categories as $sub_category)
                        <li class="text-primary list-group-item bg-transparent pt-1 pb-1">
                          <a class="btn btn-outline-primary btn-block" href="{{route('user.show_products_under_sub_category', $sub_category->id)}}">
                            {{$sub_category->sub_category}}
                          </a>
                        </li>
                    @empty
                        <li class="text-primary list-group-item bg-transparent border-bottom border-primary pt-1 pb-1">
                          No sub-categories
                        </li>
                    @endforelse
                    </ul>
                </div>

            </div>
          </div>

        @empty
            
        @endforelse
      </div>  
  
    </div>


  </div>

@endsection