<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Perrsonal Healt Asistent') }}</title>



    <!-- Fonts -->
    @include('user.inc.fonts')

    <!-- Styles -->
    @include('user.inc.styles')

</head>
<body data-spy="scroll" data-target=".navbar" data-offset="50">
    <div id="app" class="">
        
        @if (url()->current() == route('user.welcome'))
            @include('user.inc.navbars.navbar')
        @else
            @include('user.inc.navbars.secondary_navbar')
        @endif
        <main class="">
            <div class="container-fluid ">
                @yield('content')
            </div>
        </main>
        @if (url()->current() == route('user.welcome') 
            || url()->current() == route('user.show_all_clinics')
            || url()->current() == route('user.show_fitness_page')
            || url()->current() == route('user.show_psychology_page'))
            @include('user.inc.footers.footer')
        @endif

    </div>

    <!-- Global modal -->
    @include('user.inc.terms_of_use_modal')
    
    <!-- Scripts -->
    @include('user.inc.scripts')
</body>

</html>
