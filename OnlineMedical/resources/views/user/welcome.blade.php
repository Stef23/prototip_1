@extends('user.layouts.app')
@section('blade_css')
<link href="{{ asset('user/css/welcome.css') }}" rel="stylesheet">
@endsection

@section('content')

    @include('user.inc.welcome_page.header')

    @include('user.inc.welcome_page.how_it_works')

    @include('user.inc.welcome_page.our_services')

    {{-- @include('user.inc.welcome_page.statistics') --}}

    @include('user.inc.welcome_page.blogs')

    @include('user.inc.welcome_page.our_team')
    
    @include('user.inc.welcome_page.mental_health')

    @include('user.inc.welcome_page.what_our_clients_say')
    
    @include('user.inc.welcome_page.online_fitnes_advisor')

    @include('user.inc.welcome_page.online_pharmacy')

    {{-- @include('user.inc.welcome_page.sponsors') --}}

@endsection