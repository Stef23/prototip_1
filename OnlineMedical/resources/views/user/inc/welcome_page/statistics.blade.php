<div id="statistics" class="row bg-black justify-content-around align-items-center shadow-lg">
    <div class="col-md-4">
        <h1 class="">Запознајте не преку бројки</h1>
    </div>
    <div class="col-md-6">
        <div class="row">
            <div class="col-md-6">
                <h3 class="text-center"><span class="h2">2323<span class="text-primary">+</span></><br> Задоволни корисници </h3>
            </div>
            <div class="col-md-6">
                <h3 class="text-center"><span class="h2">2323<span class="text-primary">+</span></><br> Дневни посети </h3>
            </div>
        </div>
    </div>
</div>