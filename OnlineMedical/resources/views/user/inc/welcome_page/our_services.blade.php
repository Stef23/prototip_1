<div id="our_services" class="py-5 row justify-content-center align-items-center">
    <div class="col-md-6 text-center py-5">
        <h1 class="custom-tablet-screen-heading-font"> <strong> Наши услуги </strong></h1>
        <div class="custom-hr"></div>
        <p class="custom-tablet-screen-p-font">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Praesentium libero repudiandae possimus sequi! Numquam, ex amet. Praesentium ullam iusto nam, fugiat esse</p>
    </div>
    <div class="col-md-10">
        <div class="row justify-content-center align-items-center">
            
            <div class="card-deck">
                
                    <div class="card border border-primary shadow">
                        <a href="{{route('user.show_all_questionnaires')}}" class="text-decoration-none">
                            <div class="card-body">
                                <div class="services-icon text-primary display-4 p-2">
                                    <i class="fas fa-clipboard-list"></i>
                                </div>
                                <h4 class="border-bottom border-primary">Персонален медицински асистент</h4>
                                <p class="custom-tablet-screen-p-font">Пополнувате краток прашалник и веднаш добивате одговор со препорака за лек или достапна клиника за вашиот проблем</p>    
                            </div>
                        </a>
                    </div>    
                
                
                
                    <div class="card border border-primary shadow">
                        <a href="{{route('user.show_all_products')}}" class="text-decoration-none">
                            <div class="card-body">
                                <div class="services-icon text-primary display-4 p-2">
                                    <i class="fas fa-capsules"></i>
                                </div>
                                <h4 class="border-bottom border-primary">Палета на производи</h4>
                                <p class="custom-tablet-screen-p-font">Палетата на производи служи како упатство на употреба за истите и каде може да ги набавите</p>    
                            </div>
                        </a>    
                    </div>    
                

                
                    <div class="card border border-primary shadow">
                        <a href="{{route('user.show_all_clinics')}}" class="text-decoration-none">
                            <div class="card-body">
                                <div class="services-icon text-primary display-4 p-2">
                                    <i class="fas fa-clinic-medical"></i>
                                </div>
                                <h4 class="border-bottom border-primary">24/7 Клиники</h4>
                                <p class="custom-tablet-screen-p-font">Базата на клиники служи да се информирате кои клиники имаат дежурна служба 24/7, да дојдете до нивен телефонски број и локација</p>    
                            </div>
                        </a>    
                    </div>    
                
            </div>

            <div class="card-deck mt-3">
                <div class="card border border-primary shadow">
                    <a href="{{route('user.show_all_blogs')}}" class="text-decoration-none">
                        <div class="card-body">
                            <div class="services-icon text-primary display-4 p-2">
                                <i class="fas fa-book-reader"></i>
                            </div>
                            <h4 class="border-bottom border-primary">Блогови</h4>
                            <p class="custom-tablet-screen-p-font">Блоговите се напишани од медицински лица со цел да го подобрат секојдневниот живот, а воедно и да побијат некој од најчестите митови</p>    
                        </div>    
                    </a>
                </div>    

                
                    <div class="card border border-primary shadow">
                        <a href="{{route('user.show_fitness_page')}}" class="text-decoration-none">
                            <div class="card-body">
                                <div class="services-icon text-primary display-4 p-2">
                                    <i class="fas fa-dumbbell"></i>
                                </div>
                                <h4 class="border-bottom border-primary">Физичко здравје и фитнес</h4>
                                <p class="custom-tablet-screen-p-font">Блоговите се напишани од медицински лица со цел да го подобрат секојдневниот живот, а воедно и да побијат некој од најчестите митови</p>    
                            </div>
                        </a>
                    </div>    
                

                
                    <div class="card border border-primary shadow">
                        <a href="{{route('user.show_psychology_page')}}" class="text-decoration-none">
                            <div class="card-body">
                                <div class="services-icon text-primary display-4 p-2">
                                    <i class="fas fa-brain"></i>
                                </div>
                                <h4 class="border-bottom border-primary">Ментално здравје</h4>
                                <p class="custom-tablet-screen-p-font">Блоговите се напишани од медицински лица со цел да го подобрат секојдневниот живот, а воедно и да побијат некој од најчестите митови</p>    
                            </div>    
                        </a>
                    </div>    
                
            </div>
        </div>        
    </div>
</div>
