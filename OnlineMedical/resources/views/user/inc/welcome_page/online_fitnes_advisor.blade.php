<div id="online_fitnes_advisor" class="row justify-content-center align-items-center">
    <div class="col-md-10">
        <h1 class="text-center custom-tablet-screen-heading-font"> <strong> Физичко здравје и фитнес </strong></h1>
        <div class="custom-hr"></div>
        <div class="row align-items-center">
            <div class="col-md-7 my-4">
                <h1 class="text-center custom-tablet-screen-heading-font">{{$latest_fitness_blog->title}}</h1>
                <p class="custom-tablet-screen-p-font">{{Str::limit(strip_tags($latest_fitness_blog->body), 300)}}</p>
                
                <a class="btn btn-outline-primary btn-round" href="{{route('user.show_blog', $latest_fitness_blog->id)}}">Прочитај повеќе</a>
            </div>
            <div class="col-md-4 offset-md-1">
                {{-- <img src="{{asset('storage/'.$blog->cover_image)}}" class="card-img" alt="..."> --}}
                <img src="{{$latest_fitness_blog->cover_image}}" class="card-img" alt="...">
            </div>
        </div>
    </div>
</div>