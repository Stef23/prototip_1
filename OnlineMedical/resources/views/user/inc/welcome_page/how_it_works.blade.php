<div id="how_it_works" class="row justify-content-center align-items-center min-vh-100">
    <div class="col-md-10 py-5">
        <h1 class="text-center custom-tablet-screen-heading-font"> <strong> Нашата цел </strong></h1>
        <div class="custom-hr"></div>
        <div class="row align-items-center">
            <div class="col-md-4">
                <img src="{{asset('user/imgs/welcome/we_are_in_this_together.jpg')}}" class="img-fluid rounded-circle shadow-lg border-right border-primary" alt="">
            </div>
            <div class="col-md-7 offset-md-1 my-4">
                <h1 class="text-right custom-tablet-screen-heading-font">Брзо, едноставно и лесно</h1>
                <p class="text-right custom-tablet-screen-p-font">Без разлика дали сте дома или надвор од државата, наша примарна цел е да понудиме брз пристап до одговор за Вашиот проблем, во секое време. Со пополнување на краток прашалник добивате дијагноза, предлог за терапија и листа од клиники кои имаат дежурна служба 24/7.</p>
                <div class="pull-right">
                    <a class="btn btn-outline-primary btn-round " href="{{route('user.show_all_questionnaires')}}"><small>Пополнете прашалник</small> <i class="fas fa-clipboard-list"></i></a>
                </div>
            </div>
            <div class="col-md-7 my-4">
                <h1 class="text-left custom-tablet-screen-heading-font">Подигање на свеста</h1>
                <p class="text-left custom-tablet-screen-p-font">Наша втора и не-помалку важна цел е да се подигне свеста, па така исто како прашалниците, оваа страна содржи база на блогови кои се пишувани од стручни лица, а имаат за цел да Ви понудат знаење за подобрување на општото физичко и ментално здравје.</p>
                <div class="pull-left">
                    <a class="btn btn-outline-primary btn-round" href="{{route('user.show_all_blogs')}}"><i class="fas fa-book-reader"></i>
                    <small>Кон сите блогови</small>
                </a>
                </div>
            </div>
            <div class="col-md-4 offset-md-1">
                <img src="{{asset('user/imgs/welcome/sad_smily.jpeg')}}" class="img-fluid rounded-circle shadow-lg border-left border-primary" alt="">
            </div>
        </div>
    </div>
</div>
