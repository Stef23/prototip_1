<div id="what_our_clients_say" class="row shadow">
    <div class="col-md-12">
        <h1 class="text-center mt-4 custom-tablet-screen-heading-font"> <strong> Што велат нашите корисници? </strong></h1>
        <div class="custom-hr"></div>
    </div>

    <div class="w-100">
        <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
              <div class="carousel-item active">
                <img class="d-block w-100" height="300px" src="https://www.publicdomainpictures.net/pictures/30000/velka/plain-white-background.jpg" alt="First slide">
                <div class="carousel-caption text-dark d-md-block">
                    <p class="custom-tablet-screen-p-font">Lorem ipsum dolor sit amet consectetur adipisicing elit. Laborum odit vitae sequi porro, rerum quibusdam consectetur officiis doloribus perspiciatis veritatis, laudantium nostrum fugit illum, magni neque cupiditate accusantium! Rem, accusamus.</p>
                    <h1>Stefan</h1>
                </div>
              </div>
              <div class="carousel-item">
                <img class="d-block w-100" height="300px" src="https://www.publicdomainpictures.net/pictures/30000/velka/plain-white-background.jpg" alt="Second slide">
                <div class="carousel-caption text-dark d-md-block">
                    <p class="custom-tablet-screen-p-font">Lorem ipsum dolor sit amet consectetur adipisicing elit. Laborum odit vitae sequi porro, rerum quibusdam consectetur officiis doloribus perspiciatis veritatis, laudantium nostrum fugit illum, magni neque cupiditate accusantium! Rem, accusamus.</p>
                    <h1>Kiril</h1>
                </div>
              </div>
              <div class="carousel-item">
                <img class="d-block w-100" height="300px" src="https://www.publicdomainpictures.net/pictures/30000/velka/plain-white-background.jpg" alt="Third slide">
                <div class="carousel-caption text-dark d-md-block">
                    <p class="custom-tablet-screen-p-font">Lorem ipsum dolor sit amet consectetur adipisicing elit. Laborum odit vitae sequi porro, rerum quibusdam consectetur officiis doloribus perspiciatis veritatis, laudantium nostrum fugit illum, magni neque cupiditate accusantium! Rem, accusamus.</p>
                    <h1>Boban</h1>
                </div>
              </div>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
              <span class="carousel-control-prev-icon" aria-hidden="true"></span>
              <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
              <span class="carousel-control-next-icon" aria-hidden="true"></span>
              <span class="sr-only">Next</span>
            </a>
          </div>
    </div>
</div>
