<div id="blogs" class="row">
    <div class="col-md-6 offset-md-3 text-center">
        <h1 class="custom-tablet-screen-heading-font"><strong>Најнови блогови</strong></h1>
        <div class="custom-hr"></div>
        <p class="custom-tablet-screen-p-font">Lorem ipsum dolor sit amet consectetur adipisicing elit. Esse, quam?</p>
    </div>
    <div class="col-md-10 offset-md-1">
        <div class="row">
            @forelse ($blogs as $blog)
            <div class="col-lg-4 col-md-6 my-3 hvr-shrink">
                <a href="{{route('user.show_blog', $blog->id)}}" class="text-decoration-none text-dark">
                    <div class="card border-0 shadow">
                        <div class="card-body min-vh-25 p-0 cover-bg" 
                        {{-- style="background-image: url({{asset('storage/'.$blog->cover_image)}});" --}}
                        style="background-image: url({{asset($blog->cover_image)}});"
                             >
                        </div>
                        <div class="card-footer bg-white py-4">
                            <p class="h5 text-center"><strong>{{$blog->title}}</strong></p>
                        </div>
                    </div>
                </a>
            </div>
            @empty
                
            @endforelse


        </div>
    </div>
</div>