<div id="our_team" class="row bg-light">
    <div class="col-md-12 text-center">
        <h1 class="my-2 custom-tablet-screen-heading-font"><strong> Вашите онлајн херои </strong></h1>
        <div class="custom-hr"></div>
    </div>
    <div class="col-md-10 offset-md-1">
        <div class="row">
            @for ($i = 0; $i < 4; $i++)                
                <div class="col-md-3 text-center">
                    <div class="my-2">
                        <img class="img-fluid rounded" src="https://source.unsplash.com/800x600/?doctors" alt="">
                    </div>
                    <h3 class="my-2">Name</h3>
                    <p class="my-2">Title</p>
                    <hr>
                    <div>
                        <i class="mx-2 fab fa-linkedin-in"></i>
                        <i class="mx-2 fab fa-facebook-f"></i>
                        <i class="mx-2 fab fa-google-plus-g"></i>
                        <i class="mx-2 fab fa-twitter"></i>
                    </div>
                </div>
            @endfor
        </div>
    </div>
</div>