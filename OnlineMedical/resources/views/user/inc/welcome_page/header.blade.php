<header id="header" class="row min-vh-100 align-items-center cover-bg shadow" style="background-image: url('{{asset('user/imgs/welcome/doctor_laptop.jpg')}}');">
        <div id="header_headline_container" class="col-md-5 offset-md-1 text-light animated fadeInLeft">
          <h1 class="text-uppercase text-white custom-tablet-screen-heading-font">персонален <strong class="text-primary">медицински</strong> асистент</h1>  
          <p class="custom-tablet-screen-p-font">Место на кое можете најлесно да дојдете до едноставен и прецизен медицински совет!</p>
          <a class="btn btn-lg btn-outline-primary btn-round hvr-wobble-vertical" href="#how_it_works"><i class="fas fa-arrow-down"></i></a>
        </div>
        <div id="header_questionnaire_container" class="col-md-6 d-flex flex-column justify-content-around align-items-center animated fadeInRight">
          <h4 class="text-white">Пополнете прашалник </h4>
          <a class="hvr-buzz" href="{{route('user.show_all_questionnaires')}}"><i class="display-1 fas fa-clipboard-list"></i></a>
        </div>
        <div class="overlay"></div>
</header>