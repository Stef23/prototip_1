<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
<link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
{{-- <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet"> --}}
<link href="{{ asset('css/hover.css') }}" rel="stylesheet">
<link href="{{ asset('css/animate.css') }}" rel="stylesheet">
@yield('blade_css')
<link href="{{ asset('user/css/main.css') }}" rel="stylesheet">
<link href="{{ asset('user/css/footer.css') }}" rel="stylesheet">