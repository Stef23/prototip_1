<div class="container-fluid ">
    <section style="height:80px;"></section>

    <!----------- Footer ------------>
    <footer class="footer-bs">
        <div class="row">
        	<div class="col-md-3 footer-brand animated fadeInLeft">
            	<h2>Logo</h2>
                <p>Suspendisse hendrerit tellus laoreet luctus pharetra. Aliquam porttitor vitae orci nec ultricies. Curabitur vehicula, libero eget faucibus faucibus, purus erat eleifend enim, porta pellentesque ex mi ut sem.</p>
                <p>© 2014 BS3 UI Kit, All rights reserved</p>
            </div>
        	<div class="col-md-4 footer-nav animated fadeInUp">
            	<h4>Мени —</h4>
            	<div class="col-md-6">
                    <ul class="pages">
                        <li><a href="{{route('user.show_all_questionnaires')}}">Прашалници</a></li>
                        <li><a href="{{route('user.show_all_blogs')}}">Блогови</a></li>
                        <li><a href="{{route('user.show_all_products')}}">Палета на прозиводи</a></li>
                        <li><a href="{{route('user.show_all_clinics')}}">24/7 Клиники</a></li>
                        <li><a href="{{route('user.show_fitness_page')}}">Физичко здравје и фитнес</a></li>
                        <li><a href="{{route('user.show_psychology_page')}}">Ментално здравје</a></li>
                    </ul>
                </div>
            	<div class="col-md-6">
                    <ul class="list">
                        <li><a href="#">За нас</a></li>
                        <li><a href="#">Контакт</a></li>
                        <li><a href="#" data-toggle="modal" data-target="#termsOfUseModal">Услови на користење</a></li>
                        <li><a href="#">Политика на приватност</a></li>
                    </ul>
                </div>
            </div>
        	<div class="col-md-2 footer-social animated fadeInDown">
            	<h4>Следете не</h4>
            	<ul>
                	<li><a href="#">Facebook</a></li>
                	<li><a href="#">Twitter</a></li>
                	<li><a href="#">Instagram</a></li>
                	<li><a href="#">RSS</a></li>
                </ul>
            </div>
        	<div class="col-md-3 footer-ns animated fadeInRight">
            	<h4>Претплатете се</h4>
                <p>Добивајте најнови информации на вашиот мејл</p>
                <p>
                    <div class="input-group">
                      <input type="text" class="form-control" placeholder="Вашиот мејл...">
                      <span class="input-group-btn">
                        <button class="btn btn-default" type="button"><span class="glyphicon glyphicon-envelope"></span></button>
                      </span>
                    </div><!-- /input-group -->
                 </p>
            </div>
        </div>
    </footer>

</div>