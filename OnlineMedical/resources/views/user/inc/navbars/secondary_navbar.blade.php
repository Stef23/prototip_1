<nav class="navbar questionnaire-navbar navbar-expand-md navbar-dark animated fadeInDown">
    <div class="container">

      <!-- Brand -->
      <a class="navbar-brand" href="{{route('user.welcome')}}">
        <strong>ПМА</strong>
      </a>

      <!-- Collapse -->
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
        aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <!-- Links -->
      <div class="collapse navbar-collapse" id="navbarSupportedContent">

          <!-- Left -->
          <ul class="navbar-nav m-auto">
              <li class="nav-item">
                <a class="nav-link nav-link-lg" title="ПМА" href="{{route('user.show_all_questionnaires')}}"> <i class="fas fa-clipboard-list"></i></a>
              </li>
              <li class="nav-item">
                <a class="nav-link nav-link-lg" title="Производи" href="{{route('user.show_all_products')}}"> <i class="fas fa-capsules"></i></a>
              </li>
              <li class="nav-item">
                <a class="nav-link nav-link-lg" title="Клиники" href="{{route('user.show_all_clinics')}}"> <i class="fas fa-clinic-medical"></i></a>
              </li>
              <li class="nav-item">
                <a class="nav-link nav-link-lg" title="Блогови" href="{{route('user.show_all_blogs')}}"><i class="fas fa-book-reader"></i></a>
              </li>
              <li class="nav-item">
                <a class="nav-link nav-link-lg" title="Физичко здравје" href="{{route('user.show_fitness_page')}}"> <i class="fas fa-dumbbell"></i></a>
              </li>
              <li class="nav-item">
                <a class="nav-link nav-link-lg" title="Ментално здравје" href="{{route('user.show_psychology_page')}}"> <i class="fas fa-brain"></i></a>
              </li>
          </ul>

          <!-- Right -->
          <ul class="navbar-nav nav-flex-icons">
            <li class="nav-item">
              <a href="#" class="nav-link">
                <i class="fas fa-search mr-2"></i></a>
            </li>
          </ul>

      </div>

    </div>
  </nav> 