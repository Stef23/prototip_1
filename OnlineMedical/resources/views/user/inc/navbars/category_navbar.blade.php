<ul class="nav justify-content-center blog-category-nav">
    @foreach ($categories as $category)      
      <li class="nav-item">
        <a class="nav-link nav-link-sm" href="?category={{$category->id}}">{{$category->category}}</a>
      </li>
    @endforeach
</ul>