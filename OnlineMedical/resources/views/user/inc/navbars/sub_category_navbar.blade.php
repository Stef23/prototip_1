  <!-- Navbar -->
  <ul class="nav justify-content-center">
    @forelse ($categories as $category)
      <li class="nav-item dropdown">
        <a 
        class="nav-link dropdown-toggle text-info pr-3"
        href="#" id="navbarDropdownMenuLink" 
        data-toggle="dropdown" 
        aria-haspopup="true" 
        aria-expanded="false">
        {{ $category->category }}
        </a>

        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
            @forelse ($category->sub_categories as $sub_category)
                <a class="dropdown-item text-info p-2" href="?sub_category={{$sub_category->id}}">{{$sub_category->sub_category}}</a>
            @empty

            <ul class="nav navbar-nav">
                <li class="nav-item dropdown">
                    <small  class="nav-link text-info">No subcategory added</small>
                </li>
            </ul>
            @endforelse
        </div>

      </li>
      @empty

      
      @endforelse

  </ul>