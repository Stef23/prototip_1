<nav class="navbar fixed-top navbar-expand-lg navbar-dark">
    <div class="container">

      <!-- Brand -->
      <a class="navbar-brand" href="{{route('user.welcome')}}">
        <strong>ПМА</strong>
      </a>

      <!-- Collapse -->
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
        aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <!-- Links -->
      <div class="collapse navbar-collapse" id="navbarSupportedContent">

          <!-- Left -->
          <ul class="navbar-nav mr-auto">
            <li class="nav-item">
              <a class="nav-link nav-link-sm" href="#how_it_works">Нашата цел</a>
            </li>
            <li class="nav-item">
              <a class="nav-link nav-link-sm" href="#our_services">Услуги</a>
            </li>
            <li class="nav-item">
              <a class="nav-link nav-link-sm" href="{{route('user.show_all_blogs')}}">Блогови</a>
            </li>
            <li class="nav-item">
              <a class="nav-link nav-link-sm" href="{{route('user.show_psychology_page')}}">Ментално здравје</a>
            </li>
            <li class="nav-item">
              <a class="nav-link nav-link-sm" href="{{route('user.show_fitness_page')}}">Физичко здравје</a>
            </li>
            <li class="nav-item">
              <a class="nav-link nav-link-sm" href="{{route('user.show_all_products')}}">Палета на прозиводи</a>
            </li>
          </ul>

          <!-- Right -->
          <ul class="navbar-nav nav-flex-icons">
            <li class="nav-item">
              <a href="#" class="nav-link" target="_blank">
                <i class="fab fa-facebook-f"></i>
              </a>
            </li>
            <li class="nav-item">
              <a href="#" class="nav-link" target="_blank">
                <i class="fab fa-linkedin"></i>
              </a>
            </li>
            <li class="nav-item">
              <a href="#" class="nav-link">
                <i class="fas fa-search mr-2"></i>
              </a>
            </li>
          </ul>

      </div>

    </div>
  </nav>
  <!-- Navbar -->