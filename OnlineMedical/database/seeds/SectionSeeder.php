<?php

use Illuminate\Database\Seeder;

class SectionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sections = ['Медицина', 'Физичко здравје', 'Ментално здравје'];

        foreach ($sections as $section) {
            DB::table('sections')->insert([
                'section' => $section
            ]);
        }
    }
}
