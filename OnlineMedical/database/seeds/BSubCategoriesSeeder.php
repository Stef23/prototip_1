<?php

use Illuminate\Database\Seeder;

class BSubCategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory('App\BSubCategory', 10)->create();
    }
}
