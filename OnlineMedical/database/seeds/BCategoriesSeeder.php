<?php

use Illuminate\Database\Seeder;

class BCategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory('App\BCategory', 10)->create();
    }
}
