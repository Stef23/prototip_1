<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(IconSeeder::class);
        $this->call(SectionSeeder::class);
        $this->call(BCategoriesSeeder::class);
        $this->call(BSubCategoriesSeeder::class);
        $this->call(BlogsSeeder::class);
    }
}
