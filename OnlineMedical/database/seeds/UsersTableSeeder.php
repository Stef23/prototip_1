<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Maja Ivanova',
            'email' => 'maja_ivanova90@yahoo.com',
            'password' => Hash::make('password'),
            'is_admin' => 1,
        ]);
    }
}
