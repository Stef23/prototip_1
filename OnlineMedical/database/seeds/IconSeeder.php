<?php

use Illuminate\Database\Seeder;

class IconSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $icons = [
            '<i class="fas fa-allergies"></i>',
            '<i class="fas fa-band-aid"></i>',
            '<i class="fas fa-bone"></i>',
            '<i class="fas fa-biohazard"></i>',
            '<i class="fas fa-brain"></i>',
            '<i class="fas fa-burn"></i>',
            '<i class="fas fa-briefcase-medical"></i>',
            '<i class="fas fa-capsules"></i>',
            '<i class="fas fa-crutch"></i>',
            '<i class="fas fa-diagnoses"></i>',
            '<i class="fas fa-disease"></i>',
            '<i class="fas fa-head-side-cough"></i>',
            '<i class="fas fa-head-side-mask"></i>',
            '<i class="fas fa-head-side-virus"></i>',
            '<i class="fas fa-heart"></i>',
            '<i class="fas fa-lungs-virus"></i>',
            '<i class="fas fa-lungs"></i>',
            '<i class="fas fa-pills"></i>',
            '<i class="fas fa-procedures"></i>',
            '<i class="fas fa-stethoscope"></i>',
            '<i class="fas fa-syringe"></i>',
            '<i class="fas fa-tooth"></i>',
            '<i class="fas fa-user-md"></i>',
            '<i class="fas fa-viruses"></i>',
            '<i class="far fa-eye"></i>',
            '<i class="fas fa-deaf"></i>'
        ];

        foreach($icons as $icon) {
            DB::table('icons')->insert([
                'icon' => $icon,
            ]);
        }

    }
}
