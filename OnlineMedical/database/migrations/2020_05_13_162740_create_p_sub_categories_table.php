<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePSubCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('p_sub_categories', function (Blueprint $table) {
            $table->id();
            $table->string('sub_category');
            $table->unsignedBigInteger('p_category_id');
            $table->timestamps();

            $table->foreign('p_category_id')->references('id')->on('p_categories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('p_sub_categories');
    }
}
