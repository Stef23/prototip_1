<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBSubCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('b_sub_categories', function (Blueprint $table) {
            $table->id();
            $table->string('sub_category');
            $table->unsignedBigInteger('b_category_id');
            $table->timestamps();

            $table->foreign('b_category_id')->references('id')->on('b_categories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('b_sub_categories');
    }
}
