<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('p_sub_category_id');
            $table->string('name');
            $table->text('short_description');
            $table->text('description');
            $table->text('image');
            $table->boolean('promoted')->default(0);
            $table->timestamps();

            $table->foreign('p_sub_category_id')->references('id')->on('p_sub_categories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
