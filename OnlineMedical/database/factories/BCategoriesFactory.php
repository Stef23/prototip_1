<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\BCategory;
use App\Section;
use Faker\Generator as Faker;

$factory->define(BCategory::class, function (Faker $faker) {
    $section = Section::orderByRaw('RAND()')->take(1)->get();
    return [
        'category' => $faker->word(1),
        'section_id' => $section[0]->id,
    ];
});
