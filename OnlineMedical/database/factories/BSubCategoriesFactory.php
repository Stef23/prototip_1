<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\BCategory;
use App\BSubCategory;
use Faker\Generator as Faker;

$factory->define(BSubCategory::class, function (Faker $faker) {
    $category = BCategory::orderByRaw('RAND()')->take(1)->get();
    return [
        'sub_category' => $faker->word(1),
        'b_category_id' => $category[0]->id,
    ];
});
