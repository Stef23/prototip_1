<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Blog;
use App\BSubCategory;
use App\User;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(Blog::class, function (Faker $faker) {
    $sub_category = BSubCategory::orderByRaw('RAND()')->take(1)->get();
    
    return [
        'title' => $faker->sentence(5),
        'body' => $faker->sentence(600),
        'cover_image' => 'user/imgs/blogs/' . rand(1, 36) . '.jpg',
        'b_sub_category_id' => $sub_category[0]->id,
        'user_id' => User::first()->id,
        'blog_tags' => $faker->sentence(5),
    ];
});
